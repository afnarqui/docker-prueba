webpackJsonp([3],{

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqlPageModule", function() { return SqlPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sql__ = __webpack_require__(792);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SqlPageModule = /** @class */ (function () {
    function SqlPageModule() {
    }
    SqlPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sql__["a" /* SqlPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sql__["a" /* SqlPage */]),
            ],
        })
    ], SqlPageModule);
    return SqlPageModule;
}());

//# sourceMappingURL=sql.module.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SqlPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_push_push__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SqlPage = /** @class */ (function () {
    function SqlPage(navCtrl, navParams, alertControllers, platforms, modalControllers, storage, servicioCompania, servicioPush, toastControllers, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertControllers = alertControllers;
        this.platforms = platforms;
        this.modalControllers = modalControllers;
        this.storage = storage;
        this.servicioCompania = servicioCompania;
        this.servicioPush = servicioPush;
        this.toastControllers = toastControllers;
        this.auth = auth;
        this.nombreCompaniaHeader = "";
        this.nombreCargoHeader = "";
        this.data = [];
        this.Obj = [];
        this.Objcabeza = [];
        this.query = '';
        this.servicioCompania.devolverVarios(['nombreCompania', 'nombreCargo'])
            .then(function (val) {
            if (val) {
                _this.nombreCompaniaHeader = val['nombreCompania'];
                _this.nombreCargoHeader = val['nombreCargo'];
            }
        });
        this.storage.get('urlPrint').then(function (val) {
            _this.urlImpresora = val;
        });
        this.presentPromp('');
    }
    SqlPage.prototype.guardar = function () {
        this.storage.set('urlPrint', this.urlImpresora);
        alert('guardado con exito');
    };
    SqlPage.prototype.generarConsultaValidar = function () {
        this.generarConsultaSql();
    };
    SqlPage.prototype.generarConsultaSql = function () {
        var _this = this;
        if (this.query !== '') {
            this.keys = [];
            this.keyscabeza = [];
            this.Objcabeza = [];
            this.Obj = [];
            this.servicioPush.cualquiercosa(this.query)
                .subscribe(function (data) {
                var cantidad = data.length;
                console.log('cantidad');
                console.log(cantidad);
                for (var i = 0; i < cantidad; i++) {
                    var datos = data[i];
                    var cadena = '';
                    var cabeza = '';
                    for (var _i = 0, datos_1 = datos; _i < datos_1.length; _i++) {
                        var item = datos_1[_i];
                        console.log(item);
                        for (var _a = 0, _b = Object.keys(item); _a < _b.length; _a++) {
                            var key = _b[_a];
                            cadena += item[key] + "               |";
                            cabeza += key + "                     >>";
                        }
                        _this.Obj.push({ nombre: cadena });
                        cadena = '';
                    }
                    _this.keys = Object.keys(_this.Obj);
                    _this.Objcabeza = { cabeza: cabeza };
                    _this.keyscabeza = Object.keys(_this.Objcabeza);
                    cadena = '';
                    cabeza = '';
                }
            }, function (error) {
                console.log(error);
            });
        }
        else {
            console.log('esta vacio');
        }
    };
    SqlPage.prototype.presentPromp = function (resultado) {
        var _this = this;
        var alert = this.alertControllers.create({
            title: 'Login',
            inputs: [
                {
                    name: 'usuario',
                    placeholder: 'Usurio'
                },
                {
                    name: 'clave',
                    placeholder: 'Contraseña',
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        _this.presentPromp(resultado);
                    }
                },
                {
                    text: 'Entrar',
                    handler: function (data) {
                        if (data.clave === "viboafn") {
                            // this.generarConsultaSql()
                        }
                        else {
                            _this.mensajeToast('Usuario y/o clave incorrecta, intente de nuevo');
                            _this.navCtrl.setRoot('HomePage');
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    SqlPage.prototype.mensajeToast = function (mensaje) {
        var item = {
            "duration": 1000,
            "position": "buttom"
        };
        item["message"] = mensaje;
        var toast = this.toastControllers.create(item);
        toast.present();
    };
    SqlPage.prototype.cerrarSession = function () {
        this.auth.logout();
        this.navCtrl.setRoot('LoginPage');
    };
    SqlPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sql',template:/*ion-inline-start:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\sql\sql.html"*/'\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title> Sql\n\n           \n\n        <div class="end">\n\n            {{nombreCargoHeader}} >> {{nombreCompaniaHeader}}</div></ion-title>\n\n\n\n    <ion-buttons end>\n\n\n\n\n\n              \n\n        <button (click)="cerrarSession()" ion-button icon-only color="royal">\n\n            <ion-icon name="close-circle"></ion-icon>\n\n        </button>\n\n\n\n\n\n\n\n          \n\n    </ion-buttons>\n\n\n\n</ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <textarea id="texta" [(ngModel)]="query" placeholder="Ingrese el query separado de ;"></textarea> \n\n  <button ion-button block item-left (click)="generarConsultaValidar()">\n\n    <ion-icon name="save">generar consulta sql</ion-icon>\n\n  </button>\n\n  <ion-item *ngFor="let key of keyscabeza">\n\n    <ion-label>{{Objcabeza[key]}}</ion-label>\n\n  </ion-item>\n\n  <ion-item *ngFor="let key of keys">\n\n     <ion-label> {{Obj[key].nombre}}</ion-label> \n\n  </ion-item>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\sql\sql.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__["a" /* CompaniasProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_push_push__["a" /* PushProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthService */]])
    ], SqlPage);
    return SqlPage;
}());

//# sourceMappingURL=sql.js.map

/***/ })

});
//# sourceMappingURL=3.js.map