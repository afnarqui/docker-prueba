webpackJsonp([5],{

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductosDescripcionDomiciliosappPageModule", function() { return ProductosDescripcionDomiciliosappPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__productos_descripcion_domiciliosapp__ = __webpack_require__(790);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductosDescripcionDomiciliosappPageModule = /** @class */ (function () {
    function ProductosDescripcionDomiciliosappPageModule() {
    }
    ProductosDescripcionDomiciliosappPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__productos_descripcion_domiciliosapp__["a" /* ProductosDescripcionDomiciliosappPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__productos_descripcion_domiciliosapp__["a" /* ProductosDescripcionDomiciliosappPage */]),
            ],
        })
    ], ProductosDescripcionDomiciliosappPageModule);
    return ProductosDescripcionDomiciliosappPageModule;
}());

//# sourceMappingURL=productos-descripcion-domiciliosapp.module.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductosDescripcionDomiciliosappPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_domiciliosapp_domiciliosapp__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_grupos_productos_domicilios_descripcion_grupos_productos_domicilios_descripcion__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductosDescripcionDomiciliosappPage = /** @class */ (function () {
    function ProductosDescripcionDomiciliosappPage(navCtrl, navParams, viewControllers, servicioDomicilios, modalControllers, storages) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewControllers = viewControllers;
        this.servicioDomicilios = servicioDomicilios;
        this.modalControllers = modalControllers;
        this.storages = storages;
        this.data = [];
        this.productosDescripcionDomicilios = [];
        this.generarcomprarenpage = false;
        if (this.navParams.get('item')) {
            this.data = this.navParams.get('item');
            this.servicioDomicilios.buscarDomiciliosAddGruposProductosContenido(this.data['companiasId'])
                .subscribe((function (data) {
                _this.productosDescripcionDomicilios = data[0];
            }));
        }
    }
    ProductosDescripcionDomiciliosappPage.prototype.seleccionaUnValor = function (item, index) {
        this.buscarproductosDescripcionDomicilios(item, index);
        // console.log(item,index)
    };
    ProductosDescripcionDomiciliosappPage.prototype.disminuirUnProducto = function (item, index) {
        var nuevaCantidad = Number(item['cantidad']) - 1;
        // si la cantidad es menor o igual a cero debe quitar el producto
        if (nuevaCantidad <= 0) {
            //this.productosDescripcionDomicilios.splice(index, 1)
            // validar si no hay productos seleccionados
            // if(this.productosDescripcionDomicilios.length <= 0){
            //     this.realizandoUnaVenta = false
            // }
        }
        else {
            this.productosDescripcionDomicilios[index]['cantidad'] = nuevaCantidad;
        }
        this.generarcomprarenpage = true;
        // this.valorTotalVenta = this.valorTotalVenta - item['valorVenta']
        // this.valorTotalProductos = this.valorTotalProductos - item['valorProducto']
        // this.valorTotalImpuestos = this.valorTotalImpuestos - item['valorImpuesto']
    };
    ProductosDescripcionDomiciliosappPage.prototype.aumentarUnProducto = function (item, index) {
        // var noDefinido = item['cantidad']
        // if(noDefinido ==undefined && index ==undefined){
        //     this.agregarAlCarrito(item)
        //     return false
        // }
        var nuevaCantidad = Number(item['cantidad']) + 1;
        // index = this.buscarIndex(item,index)
        this.productosDescripcionDomicilios[index]['cantidad'] = nuevaCantidad;
        // let valorTotalVentaNum = 0
        // valorTotalVentaNum = Number(this.valorTotalVenta) +  Number(item['valorVenta'])
        // this.valorTotalVenta = valorTotalVentaNum
        // let valorTotalProductosNum = 0
        // valorTotalProductosNum = Number(this.valorTotalProductos) +  Number(item['valorProducto'])
        // this.valorTotalProductos = valorTotalProductosNum
        // let valorTotalImpuestosNum =  0
        // valorTotalImpuestosNum =Number(this.valorTotalImpuestos) +  Number(item['valorImpuesto'])
        // this.valorTotalImpuestos = valorTotalImpuestosNum
        // item = {}
        this.generarcomprarenpage = true;
    };
    ProductosDescripcionDomiciliosappPage.prototype.buscarproductosDescripcionDomicilios = function (item, index) {
        var validarPagina = this.generarcomprarenpage;
        if (validarPagina) {
            this.generarcomprarenpage = false;
            return;
        }
        this.storages.set('cantidad', item.cantidad);
        if (this.productosDescripcionDomicilios[index].gruposProductosDomiciliosId !== '0') {
            var modal = this.modalControllers.create(__WEBPACK_IMPORTED_MODULE_3__pages_grupos_productos_domicilios_descripcion_grupos_productos_domicilios_descripcion__["a" /* GruposProductosDomiciliosDescripcionPage */], { item: item });
            modal.present();
            modal.onDidDismiss(function (data) {
                // if (data) {
                //   data.name = "dataDynamicCrud"
                //   data.paisesId = data.paisesId
                //   data.queHago = "A"
                //   data.nombreTabla = "paises"
                //  this.servicio.actualizar(data)
                //  .subscribe(
                //    res => {
                //      this.pais = []
                //      this.cargarServicio()
                //    },
                //    err => console.log(err)
                //  )
                // }
            });
        }
    };
    ProductosDescripcionDomiciliosappPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductosDescripcionDomiciliosappPage');
    };
    ProductosDescripcionDomiciliosappPage.prototype.dismiss = function () {
        this.viewControllers.dismiss();
    };
    ProductosDescripcionDomiciliosappPage.prototype.submit = function () {
        this.viewControllers.dismiss(this.data);
    };
    ProductosDescripcionDomiciliosappPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-productos-descripcion-domiciliosapp',template:/*ion-inline-start:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\productos-descripcion-domiciliosapp\productos-descripcion-domiciliosapp.html"*/'<!--\n\n  Generated template for the ProductosDescripcionDomiciliosappPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar color="primary">\n\n        <!-- <ion-buttons left>\n\n            <button ion-button icon-only (click)="viewCtrl.dismiss()">\n\n                <ion-icon name="arrow-back"></ion-icon>\n\n            </button>\n\n        </ion-buttons> -->\n\n        <ion-buttons left>\n\n            <button navPop ion-button icon-only>\n\n              <ion-icon color="light" name="arrow-back"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n        <button class="end" ion-button icon-only (click)="pushMensaje()">\n\n            <ion-icon name="cart"></ion-icon>\n\n            <ion-badge *ngIf="pushcantidad!==0"\n\n                       class="carrito-numeroguardar">\n\n                    {{pushcantidad}}\n\n            </ion-badge>\n\n          </button>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <ion-list *ngFor="let item of productosDescripcionDomicilios; let i = index" (click)="buscarproductosDescripcionDomicilios(item,i)">\n\n        <ion-item ion-item>\n\n            <ion-thumbnail item-start>\n\n                <img [src]="item.imagen">\n\n            </ion-thumbnail>\n\n            <button ion-item>\n\n              {{ item.nombre }}\n\n\n\n            </button>\n\n\n\n\n\n            <span *ngIf="item.gruposProductosDomiciliosId===\'0\'" item-end>\n\n\n\n\n\n          </span>\n\n\n\n            <span item-end>\n\n            <button *ngIf="item.productosId!==\'0\'" ion-button icon-only (click)="disminuirUnProducto(item,i)">\n\n              <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n\n          </button>\n\n\n\n          <button *ngIf="item.productosId!==\'0\'" ion-button icon-only (click)="aumentarUnProducto(item,i)">\n\n              <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon>\n\n          </button>\n\n\n\n\n\n          <ion-badge *ngIf="item.productosId!==\'0\'" class="numero_cantidad"> {{item.cantidad}} x {{item.valorVenta | number}} </ion-badge>\n\n\n\n          <ion-badge *ngIf="item.productosId!==\'0\'" class="numero_total_precio"> {{item.cantidad * item.valorVenta | number}} </ion-badge>\n\n\n\n              <button *ngIf="item.valoresAdicionales===true" ion-button icon-only (click)="seleccionaUnValor(item,i)">\n\n                  <ion-icon name="arrow-dropright"></ion-icon>\n\n              </button>\n\n\n\n          </span>\n\n            <!-- <span *ngIf="item.gruposProductosDomiciliosId===\'0\'" item-end>\n\n\n\n                <button ion-button icon-only (click)="disminuirUnProducto(item,i)">\n\n                    <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n\n                </button>\n\n\n\n                <button ion-button icon-only (click)="aumentarUnProducto(item,i)">\n\n                    <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon>\n\n                </button>\n\n\n\n\n\n                <ion-badge class="numero_cantidad"> {{item.cantidad}} x {{item.valorVenta | number}} </ion-badge>\n\n\n\n                <ion-badge class="numero_total_precio"> {{item.cantidad * item.valorVenta | number}} </ion-badge>\n\n            </span>\n\n\n\n            <span *ngIf="item.gruposProductosDomiciliosId!==\'0\'" item-end>\n\n\n\n                <button ion-button icon-only (click)="seleccionaUnValor(item,i)">\n\n                    <ion-icon name="arrow-dropright"></ion-icon>\n\n                </button>\n\n\n\n            </span> -->\n\n\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\productos-descripcion-domiciliosapp\productos-descripcion-domiciliosapp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_domiciliosapp_domiciliosapp__["a" /* DomiciliosappProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], ProductosDescripcionDomiciliosappPage);
    return ProductosDescripcionDomiciliosappPage;
}());

//# sourceMappingURL=productos-descripcion-domiciliosapp.js.map

/***/ })

});
//# sourceMappingURL=5.js.map