webpackJsonp([13],{

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridCuentasporcobrarPageModule", function() { return GridCuentasporcobrarPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__grid_cuentasporcobrar__ = __webpack_require__(782);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GridCuentasporcobrarPageModule = /** @class */ (function () {
    function GridCuentasporcobrarPageModule() {
    }
    GridCuentasporcobrarPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__grid_cuentasporcobrar__["a" /* GridCuentasporcobrarPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__grid_cuentasporcobrar__["a" /* GridCuentasporcobrarPage */]),
            ],
        })
    ], GridCuentasporcobrarPageModule);
    return GridCuentasporcobrarPageModule;
}());

//# sourceMappingURL=grid-cuentasporcobrar.module.js.map

/***/ }),

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GridCuentasporcobrarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_por_cobrar_cuentas_por_cobrar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_caja_caja__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GridCuentasporcobrarPage = /** @class */ (function () {
    function GridCuentasporcobrarPage(navCtrl, navParams, buscarCuentasPorCobrarProvider, servicio) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.buscarCuentasPorCobrarProvider = buscarCuentasPorCobrarProvider;
        this.servicio = servicio;
        this.arrayWhereLocal = [];
        this.recibo = "";
        this.requestAutocompleteItems = function (text) {
            return _this.servicio.buscarClientesTelefono(text);
        };
    }
    GridCuentasporcobrarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GridCuentasporcobrarPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], GridCuentasporcobrarPage.prototype, "arrayWhereLocal", void 0);
    GridCuentasporcobrarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-grid-cuentasporcobrar',template:/*ion-inline-start:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\grid-cuentasporcobrar\grid-cuentasporcobrar.html"*/'<ion-content padding>\n\n    <h1>aja</h1>\n\n    <div>\n\n        <ion-card col-12>\n\n            <tag-input [(ngModel)]=\'clienteId\'\n\n                [theme]="\'minimal\'"\n\n                [placeholder]="\'Enter a new repo\'"\n\n                [onTextChangeDebounce]="500"\n\n                [secondaryPlaceholder]="\'Buscar Cliente\'"\n\n                [maxItems]="1"\n\n                [onlyFromAutocomplete]="true">\n\n                <tag-input-dropdown\n\n                    [autocompleteObservable]="requestAutocompleteItems">\n\n                    <ng-template let-item="item" let-index="index">\n\n                        {{ item.display }}\n\n                    </ng-template>\n\n                </tag-input-dropdown>\n\n            </tag-input>\n\n        </ion-card>\n\n    \n\n        andres\n\n      </div>\n\n\n\n\n\n     \n\n</ion-content>\n\n'/*ion-inline-end:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\grid-cuentasporcobrar\grid-cuentasporcobrar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_por_cobrar_cuentas_por_cobrar__["a" /* CuentasPorCobrarProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_caja_caja__["a" /* CajaProvider */]])
    ], GridCuentasporcobrarPage);
    return GridCuentasporcobrarPage;
}());

//# sourceMappingURL=grid-cuentasporcobrar.js.map

/***/ })

});
//# sourceMappingURL=13.js.map