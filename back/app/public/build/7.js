webpackJsonp([7],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalwebPageModule", function() { return PrincipalwebPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__principalweb__ = __webpack_require__(789);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PrincipalwebPageModule = /** @class */ (function () {
    function PrincipalwebPageModule() {
    }
    PrincipalwebPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__principalweb__["a" /* PrincipalwebPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__principalweb__["a" /* PrincipalwebPage */]),
            ],
        })
    ], PrincipalwebPageModule);
    return PrincipalwebPageModule;
}());

//# sourceMappingURL=principalweb.module.js.map

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrincipalwebPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_url_servicio__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PrincipalwebPage = /** @class */ (function () {
    function PrincipalwebPage(navCtrl, navParams, renderer, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.renderer = renderer;
        this.iab = iab;
        this.imagensplash = "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814473/cajaCambioDise%C3%B1o_p0flsv-min_lmjtgm.jpg";
        this.logoanimacion = "assets/imgs/logo.png";
        this.imagenbody = "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814536/tipos-de-tecnologia_gxxoxw-min_xtpshh.jpg";
        this.imagenarray = [];
        this.contadorImagen = 0;
        this.titulo = "RestApp";
        this.subtitulo = "Sart";
        this.datosPaginaHome = [];
        this.imagenslider = "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814536/tipos-de-tecnologia_gxxoxw-min_xtpshh.jpg";
        this.imagensliderarray = [];
        this.titulolider = "";
        this.descripcionslider = "";
        this.version = __WEBPACK_IMPORTED_MODULE_3__config_url_servicio__["l" /* VERSION */];
        this.nombreVersion = __WEBPACK_IMPORTED_MODULE_3__config_url_servicio__["e" /* NOMBRE_VERSION */];
        this.descripcionVersion = __WEBPACK_IMPORTED_MODULE_3__config_url_servicio__["a" /* DESCRIPCION_VERSION */];
        this.formState = 'collapsed';
        this.formStatebool = false;
        this.imagenarray = [
            { img: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814686/img_tecnosis_vety6b-min_lg4qko.jpg", titulo: "Descubre lo que podemos hacer juntos", subtitulo: "Datos en tiempo real desde cualquier lugar del mundo",
                imgslider: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814473/cajaCambioDise%C3%B1o_p0flsv-min_lmjtgm.jpg", titulolider: "Maneje sus ventas", descripcionslider: "Es rapido hacerlo" },
            { img: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814862/3_q1vxzo-min_khg46u.jpg", titulo: "El sistema administrativo ideal para su negocio", subtitulo: "Súper fácil de usar",
                imgslider: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814973/drag_xm3g7q-min_xsfv98.jpg", titulolider: "Ordene sus productos", descripcionslider: "lo haces de forma fácil y sencillo" },
            { img: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537815039/internet-velocidade_klhbsx-min_syxaj3.jpg", titulo: "¿ Quieres mejorar tus ingresos", subtitulo: "Llegamos a la necesidad de tu empresa",
                imgslider: "https://res.cloudinary.com/drqk6qzo7/image/upload/v1537815168/cierre_s2fdfm-min_gaxtt6.jpg", titulolider: "Genere su cierre con un clic", descripcionslider: "envió de correo automáticamente" }
        ];
        this.datosPaginaHome = [];
        this.datosPaginaHome = [
            { "nombre": "CajaPage", "ruta": "CajaPage", "titulo": "Caja", "subtitulo": "Ventas en tiempo real", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1511852931/caja_cs6wvs.jpg" },
            { "nombre": "MeseromeseroPage", "ruta": "MeseromeseroPage", "titulo": "Pedidos salón", "subtitulo": "Manejo desde dispositivo móvil", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534613628/mesas-min_dnglao.jpg" },
            { "nombre": "SeguridadPage", "ruta": "SeguridadPage", "titulo": "Seguridad", "subtitulo": "Información segura", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534611819/seguridad-min_dwzsgg.jpg" },
            { "nombre": "ReportesPage", "ruta": "ReportesPage", "titulo": "Informes", "subtitulo": "Esadisticas de ventas", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534470788/informes-min_agpenr.jpg" }
        ];
        this.startTimer();
    }
    // https://res.cloudinary.com/drqk6qzo7/image/upload/v1537538907/tecnologia-lapiz_n42yng.jpg
    PrincipalwebPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad PrincipalwebPage');
        setTimeout(function () {
            _this.renderer.setElementStyle(_this.myboton.nativeElement, 'top', '55px');
            _this.renderer.setElementStyle(_this.myboton2.nativeElement, 'top', '155px');
        }, 2000);
    };
    PrincipalwebPage.prototype.startTimer = function () {
        var _this = this;
        this.timerVar = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].interval(3000).subscribe(function (x) {
            var contador;
            for (var i = 0; i < _this.imagenarray.length; i++) {
                if (_this.contadorImagen === 2) {
                    _this.contadorImagen = 0;
                    contador = 2;
                    // this.contadorImagen+=1
                }
                else {
                    contador = _this.contadorImagen === 0 ? 0 : _this.contadorImagen;
                    _this.contadorImagen += 1;
                }
                _this.imagenbody = _this.imagenarray[contador].img;
                _this.titulo = _this.imagenarray[contador].titulo;
                _this.subtitulo = _this.imagenarray[contador].subtitulo;
                _this.imagenslider = _this.imagenarray[contador].imgslider;
                _this.titulolider = _this.imagenarray[contador].titulolider;
                _this.descripcionslider = _this.imagenarray[contador].descripcionslider;
                i = _this.imagenarray.length + 1;
            }
        });
    };
    PrincipalwebPage.prototype.cambiarStyle = function () {
        // this.renderer.setElementStyle(this.myboton.nativeElement,'opacity','0')
        // this.renderer.setElementStyle(this.myboton.nativeElement,'padding','50px')
        this.renderer.setElementStyle(this.myboton.nativeElement, 'top', '0px');
    };
    PrincipalwebPage.prototype.login = function () {
        this.navCtrl.setRoot('LoginPage');
    };
    PrincipalwebPage.prototype.toggleForm = function () {
        this.formState = (this.formState == 'collapsed') ? 'expanded' : 'collapsed';
        this.formStatebool = !this.formStatebool;
    };
    PrincipalwebPage.prototype.icon = function () {
        return (this.formState == 'collapsed') ? 'contacts' : 'arrow-dropleft-circle';
    };
    PrincipalwebPage.prototype.label = function () {
        return (this.formState == 'collapsed') ? 'Contáctanos' : 'Ocultar contacto';
    };
    PrincipalwebPage.prototype.whatsapp = function () {
        debugger;
        this.iab.create("https://web.whatsapp.com/", '_system');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myboton', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", Object)
    ], PrincipalwebPage.prototype, "myboton", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myboton2', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", Object)
    ], PrincipalwebPage.prototype, "myboton2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"])
    ], PrincipalwebPage.prototype, "slides", void 0);
    PrincipalwebPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-principalweb',template:/*ion-inline-start:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\principalweb\principalweb.html"*/'<ion-header >\n\n		<ion-navbar color="menuinicial">\n\n\n\n			<ion-title> \n\n					<div class="contacto">\n\n							<button type="button" class="large-padding pointer no-background light-text" \n\n								(click)="toggleForm()">\n\n										{{label()}}\n\n										<ion-icon name={{icon()}}></ion-icon>\n\n								\n\n											\n\n								</button> \n\n							</div>\n\n			</ion-title>\n\n				<!-- <div class="demo" (click)="login()"> Iniciar sesión</div></ion-title> -->\n\n			<ion-buttons end>\n\n					<button ion-button\n\n					(click)="login()">\n\n					Iniciar sesión\n\n								\n\n					</button> \n\n				<a class="DEMO"  href="https://marvelapp.com/9hdab37"> <ion-icon name="search"></ion-icon></a> \n\n				\n\n					\n\n			</ion-buttons>\n\n\n\n	</ion-navbar>\n\n \n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content  >\n\n\n\n		<div *ngIf="formStatebool">\n\n			<ion-item>\n\n					<button ion-button\n\n								(click)="whatsapp()">\n\n								3207330254   \n\n								 <ion-icon name="logo-whatsapp" (click)="whatsapp()"></ion-icon> \n\n								</button>\n\n			</ion-item>\n\n				<ion-item>\n\n						<ion-label style="text-align:left;color:#5c2b43">Andrés Felipe Naranjo Quintero</ion-label>	\n\n				</ion-item>\n\n					<ion-item>\n\n							\n\n							<a style="text-align:left;color:#5c2b43;text-decoration:none"  href="mailto:admin@viboafn.com">admin@viboafn.com  <ion-icon name="mail-open"></ion-icon></a>\n\n						</ion-item>                  \n\n						<ion-item>\n\n							\n\n								<img style="text-align:center" src="https://res.cloudinary.com/drqk6qzo7/image/upload/v1537814050/logo-min_nxr4xl.jpg"/>\n\n							</ion-item>    \n\n\n\n				 \n\n				\n\n		</div> \n\n	\n\n				<ion-card class="card-background-page" >\n\n			\n\n					<img class="splash-screen" src="{{imagenbody}}"/>\n\n					<div class="card-titlederecha">Login</div> \n\n				   <div class="card-title" #myboton>{{titulo}}</div> \n\n				\n\n				  <div class="card-subtitle"  #myboton2>{{subtitulo}}</div>\n\n				</ion-card>\n\n\n\n\n\n				<ion-grid >\n\n						<ion-row >\n\n							<ion-col class="media">\n\n									<br>\n\n								\n\n								<div style="text-align:center">\n\n										<h1>SART es la herramienta </h1>\n\n									<h1>creada pensando en los </h1>\n\n								 \n\n								 <h1><span class="text-blue">empresarios y/o emprendedores</span> </h1>\n\n							\n\n								 <h1>que desean hacer su vida más fácil utilizando </h1>\n\n								\n\n								 <h1>tecnologías súper amigables de usar y</h1>\n\n						\n\n								 <h1>adaptadas a cualquier plataforma</h1>\n\n								</div>\n\n							</ion-col>\n\n							<ion-col class="media">\n\n									\n\n								<!-- <img class="imgtam" src="https://res.cloudinary.com/drqk6qzo7/image/upload/v1537747081/sartpc1-min_zhjety.jpg"/>	 -->\n\n								<img class="imgtam" src="https://res.cloudinary.com/drqk6qzo7/image/upload/v1537813881/sartpc3-min_nozma5.jpg"/>	\n\n								\n\n								</ion-col>\n\n						</ion-row>\n\n					\n\n					</ion-grid>\n\n\n\n			<h1>Vive la experiencia de <p>administrar de forma fácil</p> tu negocio</h1>\n\n\n\n\n\n			<ion-grid class="fondogrid" >\n\n						<ion-row>\n\n						  <ion-col  col-12 col-md-6 col-lg-4 col-xl-3 *ngFor="let item of datosPaginaHome">\n\n							<ion-card class="afn animate clic col">\n\n							  <ion-card-header style="text-align:center">{{item.titulo}}</ion-card-header>\n\n							  <ion-card-content>\n\n									<img src="{{item.imagen}}" width="100" height="100"/>\n\n									<div  style="text-align:center">{{item.subtitulo}}</div>\n\n							  </ion-card-content>\n\n							</ion-card>\n\n						  </ion-col>\n\n						</ion-row>\n\n					  </ion-grid> \n\n\n\n			\n\n							<h1>¿ Que deseas hacer hoy ?</h1>\n\n\n\n						<ion-slides pager>\n\n						<ion-slide>\n\n						  <img class="maximoslider" src="{{imagenslider}}"  width="100" height="100"/>\n\n						  <h2>{{titulolider}}</h2>\n\n						  <p>{{descripcionslider}}</p>\n\n						</ion-slide>\n\n						\n\n					  </ion-slides>\n\n\n\n						\n\n		\n\n					 <footer>\n\n							<h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n\n							<h6>Company @RestApp &copy; 2018</h6>\n\n							<h6><a  href="mailto:admin@viboafn.com"><h6>admin@viboafn.com</h6></a></h6>\n\n							<h3 style="text-align:center">Versión {{version}} <span></span></h3>\n\n					</footer>	 \n\n\n\n				\n\n</ion-content>\n\n'/*ion-inline-end:"C:\project\san-2021-05-29\SART-WWW-DEV\src\pages\principalweb\principalweb.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], PrincipalwebPage);
    return PrincipalwebPage;
}());

//# sourceMappingURL=principalweb.js.map

/***/ })

});
//# sourceMappingURL=7.js.map