/* inicio forma de debuggear en visual code 

ANDRES NARANJO 2017-11-15
se debe crear un server1.js dentro de la carpeta src con las siguientes lineas


/*

inicio server1.js



import express from 'express';


const app = express();
import fs from 'fs';

import routes from './routes/main.routes';



import config from './config.json';

import cors from 'cors'
import bodyParser from 'body-parser'

import dbSql from './helpers/db';

app.use(cors({
    origin: true,
    credentials: true
}));

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

var http = require('http').Server(app);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// app.use('/v2/api/', routesV2);

app.use('/public/api/', routes);




app.get('/', function(req, res){
  res.sendfile('index.html');
});


  http.listen(config.portSockect, function(){
    console.log('listening on *:'+config.portSockect);
  });



  const server = app.listen(config.port, () => {

    const {address, port} = server.address();

    console.log(`Corriendo en el puerto: ${port}`);
  });

  app.use(function (req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      var imgs = req.path.search('imagenes/');
      if(imgs != -1){
        var img = fs.readFileSync('./Imagenes/defecto.jpg');
        res.writeHead(200, {'Content-Type': 'image/gif' });
        res.end(img, 'binary');
        //res.send({ message: err.message,error:err });
      }else {
        //console.log('No se encuentra la imagen');
      }
      next(err);
  });

  if (app.get('env') === 'development') {

      app.use(function (err, req, res, next) {
          res.status(err.status || 500);
          /*res.send('error', {
              message: err.message,
              error: err
          });*/
          res.send({ message: err.message,error:err });
      });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.send({
          message: err.message,
          error: {}
      });
  });








fin server1.js

se debe crear un raper index.js en la carpeta raiz con las siguientes lineas

/*  inicio index.js


debugger
require("babel-register")
require('./src/server1')

/*  fin index.js

*/

se debe crear dentro de src un archivo con nombre .babelrc 
con las siguientes lineas

/* inicio .babelrc 
{
 "presets" : [ "es2015" ]
} 

*/
/* fin .babelrc 

se deben instalar las siguientes dependencias 

/* inicio dependencias

    npm install --save-dev babel-preset-es2015
    
    npm install --save-dev babel-register


fin dependencias */


/* cambiar en el package.json start ojo es solo para debuggear ya que seguimos transpilando el código */

    para debuggear debe quedar asi
     "start": "node index.js"

     "start": "node_modules/nodemon/bin/nodemon.js -- node_modules/babel/bin/babel-node.js src/server.js"
    
/* cambiar en el package.json start ojo es solo para debuggear ya que seguimos transpilando el código */
/* fin forma de debuggear en visual code 

