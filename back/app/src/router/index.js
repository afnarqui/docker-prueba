var express =require( 'express');
// const notifier = require('node-notifier');
const router = express.Router();

import Auth from '../controllers/auth'
const auth = new Auth()

import Productos from '../controllers/productos'
const productos = new Productos()


import GruposProductos from '../controllers/gruposProductos'
const gruposProductos = new GruposProductos()


const isAuth = require( '../middlewares/auth');

var query =require( '../controllers/queryController');

import ProductosElaborados from '../controllers/productosElaborados'
const productosElaborados = new ProductosElaborados()


// conexion a base de datos para utilizar procedimientos
import DB from '../conexion/db'

router.get('/query', (req, res) => {
  query.get(req, res, req.body)
})



// router.get('/notificacion', (req, res) => {
 

// notifier.notify({
//   'title': req.query.titulo,
//   'subtitle': 'Mensaje cargar....',
//   'message': req.query.mensaje,
//   'icon': 'ico.png',
//   'contentImage': 'ico.png',
//   'sound': 'ding.mp3',
//   'wait': true
// });
//   res.send( req.query.mensaje)
// })

router.post('/query', (req, res) => {
  debugger
  query.post(req, res, req.query)
})

router.post('/query/:pa', (req, res) => {
 
  query.postr(req, res, req.query)
})

router.put('/query', (req, res) => {
  debugger
  query.put(req, res, req.query)
})

router.delete('/query', (req, res) => {
  debugger
  query.delete(req, res, req.query)
})

// api para registro y login de usuarios
router.post('/registro', auth.signUp.bind(auth))
router.post('/login', auth.singIn.bind(auth))

// api para crud de productos
router.post('/producto', isAuth, productos.crear.bind(productos))
router.put('/producto', isAuth, productos.editar.bind(productos))
// api para crud de grupos productos
router.post('/gruposProductos', isAuth, gruposProductos.crear.bind(gruposProductos))
router.put('/gruposProductos', isAuth, gruposProductos.editar.bind(gruposProductos))

router.get('/proceso/:nombreProcedimiento', (req, res) => {
  // nombre del procedimiento almacenado
  let nombre = req.params.nombreProcedimiento
  // parametros que se le van a enviar al procedimiento
  let parametros = req.query
  // crear conexion
  let conexion = new DB()
  // realizar el proceso
  conexion.Procedure(nombre, parametros)
  .then( respuesta => {
    res.send(respuesta)
  })
  .catch( error => {
    res.status(500).send(error)
  })

})

router.post('/proceso/:nombreProcedimiento', isAuth, (req, res) => {
  // nombre del procedimiento almacenado
  let nombre = req.params.nombreProcedimiento
  // parametros que se le van a enviar al procedimiento
  let parametros = req.body
  // crear conexion
  let conexion = new DB()
  // realizar el proceso
  conexion.Procedure(nombre, parametros)
  .then( respuesta => {
    res.send(respuesta)
   
  })
  .catch( error => {
    res.status(500).send(error)

  })

})

router.post('/procesos/:nombreProcedimiento', (req, res) => {
  // nombre del procedimiento almacenado
  let nombre = req.params.nombreProcedimiento
  // parametros que se le van a enviar al procedimiento
  let parametros = req.body
  // crear conexion
  let conexion = new DB()
  // realizar el proceso
  conexion.Procedure(nombre, parametros)
  .then( respuesta => {
    res.send(respuesta)
  
  })
  .catch( error => {
    res.status(500).send(error)
   
  })

})



router.get('/hola', (req, res) => {
  res.send('hola')
})
import json2xls from 'json2xls'
import fs from 'fs'

// router.post('/informe',function(req,res){
//   debugger
//   //  let valor = req.params.data
//    valor = JSON.parse(valor)
   
//    let valor = req.query['data']
//    var json =[]
//    for(let i=0;i<valor.length;i++){
//     json.push({nombre:valor[i].nombre,apellido:valor[i].apellido})
//    }
  
//     var xls = json2xls(json);
//     console.log(json)
//    fs.writeFile('c:\\bd\\data.xlsx', xls,'binary')   
  
  
//   })
router.post('/informes',function(req,res){


  let valor = req.body.data
  valor = JSON.parse(valor)
  let obj
   
  if(typeof valor == "string"){
    obj = JSON.parse(valor)
  }else{
     obj = valor
  }
  
  let nombre = req.body.nombre
  let ruta = req.body.ruta
  if(ruta===undefined){
    ruta = "c:\\SART\\PRODUCCION\\www\\assets\\"
  }
  var xls = json2xls(obj);
 
  // fs.writeFile(`c:\\MICAFEV107\\www\\assets\\${nombre}.xlsx`, xls,'binary')
  fs.writeFile(`${ruta}${nombre}.xlsx`, xls,'binary')

// router.post('/informes',function(req,res){


//   let valor = req.body.data
//   valor = JSON.parse(valor)
//   let obj = JSON.parse(valor)
//   let nombre = req.body.nombre
//   let ruta = req.body.ruta
//   var xls = json2xls(obj);
 
//   // fs.writeFile(`c:\\MICAFEV107\\www\\assets\\${nombre}.xlsx`, xls,'binary')
//   fs.writeFile(`${ruta}${nombre}.xlsx`, xls,'binary')

//    var json =[]
   

//   var mapaResultado = new Map();
//   let valores = '';
 
//   for (var i in obj) {

//     let dataAfn = Object.keys(obj[i])
   
//      for(var x in dataAfn){
    
//       mapaResultado.set(x,dataAfn[x]);
//      }
//  }
// for (let [key, value] of mapaResultado) {
 
//     valores = valores + `"${key}":"${value}",`;
// }

// var jsoncadena = valores;
// jsoncadena = jsoncadena.substr(jsoncadena.length - 1, 1) == ',' ? jsoncadena.substring(0, jsoncadena.length - 1) : jsoncadena;
// let nuevovalorjson =  "{" + jsoncadena + "}";

// let a = `[{${jsoncadena}}]`
// let b = JSON.parse(a)
// let c = Object.keys(b[0])
// let cadenaNuevaAfn = ''
// for(let x =0;x<obj.length;x++){
  
//   for(let z=0;z<c.length;z++){
//     cadenaNuevaAfn+= `"${b[0][z]}":"${obj[x][b[0][z]]}",`
    
    
//   }
  
//   var jsoncadena = cadenaNuevaAfn;
//   jsoncadena = jsoncadena.substr(jsoncadena.length - 1, 1) == ',' ? jsoncadena.substring(0, jsoncadena.length - 1) : jsoncadena;
//   let dataafnjson =  "{" + jsoncadena + "}";

//   let afn = `[${dataafnjson}]`
//   debugger
//   let afndos = JSON.parse(afn)
//   debugger
//   json.push(afndos)
//   cadenaNuevaAfn  = ''
// }
// debugger

// let dataRealafn = json[0]

//     var xls = json2xls(dataRealafn);
 
//    fs.writeFile(`c:\\MICAFEV107\\www\\public\\assets\\${nombre}.xlsx`, xls,'binary')   

  })

  // // // router.post('/informe',function(req,res){
  // // //   debugger
  // // //   //  let valor = req.params.data
  // // //   let valor = req.body.data
  // // //   // let valor = JSON.parse(req.body.data)
  // // //   let nombre = req.body.nombre
  // // //    debugger
  // // //   //  let valor = req.query['data']
  // // //    var json =[]
  // // //    debugger
  // // //    for(let i=0;i<valor.length;i++){
  // // //     json.push({reciboCaja:valor[i].reciboCaja})
  // // //    }
    
  // // //     var xls = json2xls(json);
   
  // // //    fs.writeFile(`c:\\MICAFEV107\\www\\public\\assets\\${nombre}.xlsx`, xls,'binary')   
  // // //   // fs.writeFile('..../file:///C:/MICAFEV107/www/public/assets/data1.xlsx' , xls,'binary')   
      
  // // //   })
  
    router.get('/informe',function(req,res){
      debugger
      //  let valor = req.params.data
      let valor = req.query['data']
      valor = JSON.parse(valor)
      let nombre = req.query.nombre
       debugger
      //  let valor = req.query['data']
       var json =[]
       debugger
       for(let i=0;i<valor.length;i++){
        json.push({reciboCaja:valor[i].reciboCaja})
       }
      
        var xls = json2xls(json);
     
       fs.writeFile(`c:\\MICAFEV107\\www\\public\\assets\\${nombre}.xlsx`, xls,'binary')   
      // fs.writeFile('..../file:///C:/MICAFEV107/www/public/assets/data1.xlsx' , xls,'binary')   
        
      })


      router.get('/informes',function(req,res){
        debugger
        //  let valor = req.params.data
      
        })
  


router.post('/productosElaborados', isAuth, productosElaborados.crear.bind(productosElaborados))
router.put('/productosElaborados', isAuth, productosElaborados.editar.bind(productosElaborados))


module.exports = router
