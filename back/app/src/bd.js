// const sql =  require('mssql');


 
//     select =  (query, config) => {

//     return new Promise((resolve, reject) => {
//         const pool = new sql.ConnectionPool(config, err => {

//             if (err) {
//                 reject([err]);
//             }

          
//             var request = new sql.Request(pool);
//             request.query(query, (errors, result) => {

//                 if (errors) {
//                     pool.close();
//                     reject([errors]);
//                 }

//                 if (result) {
//                     pool.close();
//                     resolve(result['recordsets'][0]);
//                 } else {
//                     pool.close();
//                     resolve([]);
//                 }

//             });

//         });

//     });
// }

// query = (name, params, config,queHago,nombreTabla) => {

//     var configConection = config;

//     return new Promise((resolve, reject) => {
//         const pool = new sql.ConnectionPool(configConection, err => {
//             if (err) {
//                 reject([err]);
//             }

//             var request = new sql.Request(pool);
//             let json = []
//             let valores = ''

//             if (queHago !== "b"){
//                 for(let [key, value] of params){
//                     valores= valores + `"${key}":"${value}",`
//                   }
//                   var jsoncadena= valores;  
//                   jsoncadena=jsoncadena.substr(jsoncadena.length-1,1)==','?jsoncadena.substring(0,jsoncadena.length-1):jsoncadena; 
//                   let nuevovalorjson = "{" + jsoncadena + "}"
//                   //nuevovalorjson = JSON.parse(nuevovalorjson)                
//                   ///json.push(nuevovalorjson)
//                   request.input('json', nuevovalorjson);
//                   request.input('queHago', queHago);
//                   request.input('nombreTabla', nombreTabla);
//             }

//             request.execute(name, (errors, result) => {
//                 if (errors) {
//                     pool.close();
//                     reject([errors]);
//                 }

//                 if (result) {
//                     pool.close();
//                     resolve(result['recordsets'][0]);
//                 } else {
//                     pool.close();
//                     resolve([]);
//                 }
//             });

//         });
//     });
// }

// module.exports = {
//     consulta
// }

// }

const  sql = require('mssql');

// se cambia el config.json por config.js por el cambio de conexion JLV 18/10/2017
//const config from '../config.json';
import config from './config.js'

const configSql = config.sql;

export default class SqlServer {
  constructor() {
    this.config = configSql;
  }

  //ejecutar contulta
  select (name, params, config,queHago,nombreTabla) {
    return new Promise( (resolve, reject) => {
      const pool1 = new sql.ConnectionPool(this.config, err => {
        
        if(err){
          console.log('===================ERROR-CON=================');
          console.log(err);
          console.log('===================ERROR-CON=================');
          reject([]);
        }
    
        // Query
        var request = new sql.Request(pool1);
        request.query(name, (errors, result) => {

          if(errors){
            console.log('================ERROR-QUERY====================');
            console.log(name,errors);
            console.log('================ERROR-QUERY====================');
            reject([]);
          }

          if(result){
            resolve(result['recordsets']);
          }else {
            resolve([]);
          }

        });
    
      });
    
    });
  }

  //ejecutar procedimientos alamacenados
  query (name, params, config,queHago,nombreTabla) {
    var configConection = this.config; 
    
    if(newConfig){
      configConection = newConfig;
    }

    return new Promise( (resolve, reject)  => {
      const pool2 = new sql.ConnectionPool(configConection, err => {
        if(err){
          console.log('===================ERROR-CON=================');
          console.log(err);
          console.log('===================ERROR-CON=================');
          reject([]);
        }

        //Procedure
        var request = new sql.Request(pool2);

        for (var i in params) {
          if(typeof params[i] ==  'object'){
            request.input(i, params[i][0]);
          }else{
            request.input(i, params[i]);
          }
        }

        request.execute(name, (errors, result)=> {
          if(errors){
            console.log('================ERROR-PROCEDURE====================');
            console.log(name,errors);
            console.log('================ERROR-PROCEDURE====================');
            reject([]);
          }

          if(result){
            resolve(result['recordsets']);
          }else {
            resolve([]);
          }
        });

      });
    });
  }

}