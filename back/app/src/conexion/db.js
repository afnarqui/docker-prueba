import sql from 'mssql';

// se cambia el config.json por config.js por el cambio de conexion JLV 18/10/2017
//import config from '../config.json';
import config from '../config.js';

const configSql = config.sql;

export default class SqlServer {
  constructor() {
    this.config = configSql;
  }

  //ejecutar contulta
  Query (query) {
    return new Promise( (resolve, reject) => {
      const pool1 = new sql.ConnectionPool(this.config, err => {
        
        if(err){
          console.log('===================ERROR-CON=================');
          console.log(err);
          console.log('===================ERROR-CON=================');
          reject([]);
        }
    
        // Query
        var request = new sql.Request(pool1);
        request.query(query, (errors, result) => {

          if(errors){
            console.log('================ERROR-QUERY====================');
            console.log(query,errors);
            console.log('================ERROR-QUERY====================');
            reject([]);
          }

          if(result){
            resolve(result['recordsets']);
          }else {
            resolve([]);
          }

        });
    
      });
    
    });
  }

  //ejecutar procedimientos alamacenados
  Procedure (name, params, newConfig) {
    var configConection = this.config; 
    
    if(newConfig){
      configConection = newConfig;
    }

    return new Promise( (resolve, reject)  => {
      const pool2 = new sql.ConnectionPool(configConection, err => {
        if(err){
          console.log('===================ERROR-CON=================');
          console.log(err);
          console.log('===================ERROR-CON=================');
          reject([]);
        }

        //Procedure
        var request = new sql.Request(pool2);

        for (var i in params) {
          if(typeof params[i] ==  'object'){
            request.input(i, params[i][0]);
          }else{
            request.input(i, params[i]);
          }
        }

        request.execute(name, (errors, result)=> {
          if(errors){
            console.log('================ERROR-PROCEDURE====================');
            console.log(name,errors);
            console.log('================ERROR-PROCEDURE====================');
            reject([]);
          }

          if(result){
            resolve(result['recordsets']);
          }else {
            resolve([]);
          }
        });

      });
    });
  }

  //estar pendiente para emitir eventos cuando haya cambios en una tabla
  ListenSocket (){
    return sql.connect(this.config).then( (conn) => {

      let request = new sql.Request(conn);
      request.stream = true; // You can set streaming differently for each request
      request.query('select * from colores'); // or request.execute(procedure);

      request.on('recordset', function(columns) {
        // Emitted once for each recordset in a query
        console.log('afectado en recordset');
        conn.close();
      });

      request.on('row', function(row) {
        // Emitted for each row in a recordset
        console.log('afectado en row');
        conn.close();
      });

      request.on('error', function(err) {
        // May be emitted multiple times
        console.log('afectado en error');
        conn.close();
      });

      request.on('done', function(affected) {
        // Always emitted as the last one
        console.log('afectado en done');
        conn.close();
      });


    }).catch(function(err) {
      return err;
    });

  }


    //ejecutar procedimientos alamacenados
    ProcedureAFN (name, params, newConfig) {
      var configConection = this.config; 
      
      if(newConfig){
        configConection = newConfig;
      }
  
      return new Promise( (resolve, reject)  => {
        const pool2 = new sql.ConnectionPool(configConection, err => {
          if(err){
            console.log('===================ERROR-CON=================');
            console.log(err);
            console.log('===================ERROR-CON=================');
            reject([]);
          }
  
          //Procedure
          var request = new sql.Request(pool2);
  
          for (var i in params) {
            if(typeof params[i] ==  'object'){
              request.input(i, params[i][0]);
            }else{
              request.input(i, params[i]);
            }
          }
  
          request.execute(name, (errors, result)=> {
            if(errors){
              console.log('================ERROR-PROCEDURE====================');
              console.log(name,errors);
              console.log('================ERROR-PROCEDURE====================');
              request.close()
              reject([]);
            }
  
            if(result){
              resolve(result['recordsets']);
              request.close()
            }else {
              resolve([]);
              request.close()
            }
          });
  
        });
      });
    }


}
