webpackJsonp([7],{

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrincipalPageModule", function() { return PrincipalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__principal__ = __webpack_require__(770);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PrincipalPageModule = (function () {
    function PrincipalPageModule() {
    }
    return PrincipalPageModule;
}());
PrincipalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__principal__["a" /* PrincipalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__principal__["a" /* PrincipalPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__principal__["a" /* PrincipalPage */]
        ]
    })
], PrincipalPageModule);

//# sourceMappingURL=principal.module.js.map

/***/ }),

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrincipalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_caja_caja__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_electron__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__versionamiento_versionamiento__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PrincipalPage = (function () {
    function PrincipalPage(navCtrl, navParams, servicioCaja, servicioCompanias, storage, _electronService, AlertControllers, toastControllers, modalControllers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.servicioCaja = servicioCaja;
        this.servicioCompanias = servicioCompanias;
        this.storage = storage;
        this._electronService = _electronService;
        this.AlertControllers = AlertControllers;
        this.toastControllers = toastControllers;
        this.modalControllers = modalControllers;
        this.imagensplash = !__WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["d" /* MODO_DESCONECTADO */] ? "assets/imgs/3.jpg" : "assets/imgs/3_desconectado.jpg";
        this.logoanimacion = !__WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["d" /* MODO_DESCONECTADO */] ? "assets/imgs/logo.png" : "assets/imgs/logo_desconectado.png";
        this.image = null;
        this.version = __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["k" /* VERSION */];
        this.nombreVersion = __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["e" /* NOMBRE_VERSION */];
        this.descripcionVersion = __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["a" /* DESCRIPCION_VERSION */];
    }
    PrincipalPage.prototype.ionViewDidLoad = function () {
        // this.app.getRootNav().setRoot('PrincipalPage');
    };
    PrincipalPage.prototype.cerrarSession = function () {
        this.navCtrl.setRoot('LoginPage');
    };
    PrincipalPage.prototype.subirVentas = function () {
        var valores = [
            { "nombre": 'productos', 'ruta': __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], 'nombreProcedimiento': 'guardarproductosDesconectado' },
            { "nombre": 'gruposProductos', 'ruta': __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], 'nombreProcedimiento': 'guardarGruposProductosDesconectado' },
            { "nombre": 'productosPorGrupos', 'ruta': __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], 'nombreProcedimiento': 'guardarProductosPorGrupo' }
        ];
        var data = this.servicioCaja.buscarModoDesconectados(valores);
        //  let data =  this.servicioCaja.buscarVentasModoDesconectado(RUTA_MODO_DESCONECTADO)
        if (data) {
            this.AlertControllers.create({
                title: 'subir datos',
                message: 'la información de los datos se subió de forma exitosa',
                buttons: ['ok']
            }).present();
        }
    };
    // subirVentasDesconect(){
    //   let valores = [
    //     {"nombre":'productos','ruta':RUTA_MODO_DESCONECTADO,'nombreProcedimiento':'guardarGruposProductosDesconectado'},
    //     {"nombre":'gruposProductos','ruta':RUTA_MODO_DESCONECTADO,'nombreProcedimiento':'guardarGruposProductosDesconectado'}
    //   ]
    //   let result  = this._electronService.ipcRenderer.sendSync('buscar-modo-desconectado',valores)
    //   if(result){
    //     this.servicioCaja.guardarProductosDesconectado(result)
    //       .subscribe((data)=>{
    //         let result = this._electronService.ipcRenderer.sendSync('eliminar-modo-desconectado',RUTA_MODO_DESCONECTADO)
    //         if(result){
    //           this.mensajeGeneral('Guardar información','la información de los datos se subió de forma exitosa')
    //         }
    //       },(error)=>{
    //         console.log(error)
    //         this.mensajeGeneral('Guardar información','la información no se guardao verifique..')
    //       })
    //   }
    // }
    PrincipalPage.prototype.subirVentasDesconect = function () {
        var _this = this;
        var valores = [
            { "nombre": "productos", "ruta": __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], "nombreProcedimiento": "guardarproductosDesconectado" },
            { "nombre": "gruposProductos", "ruta": __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], "nombreProcedimiento": "guardarGruposProductosDesconectado" },
            { "nombre": "productosElaborados", "ruta": __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], "nombreProcedimiento": "guardarProductosElaboradosDesconectado" },
            { "nombre": "ProductosPorGrupos", "ruta": __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], "nombreProcedimiento": "guardarProductosPorGruposDesconectado" }
        ];
        var cuantos = valores.length - 1;
        var cadenaMostrar = '';
        var valoresCantidadReal = 0;
        var soloUnaVez = 0;
        var _loop_1 = function (i) {
            var nombre = valores[i].nombre;
            var ruta = valores[i].ruta;
            var nombreProcedimiento = valores[i].nombreProcedimiento;
            var valoresReales = [];
            valoresReales.push({ nombre: nombre, ruta: ruta, nombreProcedimiento: nombreProcedimiento });
            var result = this_1._electronService.ipcRenderer.sendSync('buscar-modo-desconectado', valoresReales);
            cadenaMostrar += nombre + "  ";
            if (valoresCantidadReal < 0) {
                valoresCantidadReal = 0;
            }
            else {
                if (result.length == 0 && valoresCantidadReal > 0) {
                    valoresCantidadReal -= 1;
                }
            }
            if (result.length > 0) {
                valoresCantidadReal += 1;
                var valorescantidad = result.length;
                for (var x = 0; x < valorescantidad; x++) {
                    var resultadoNuevo = result[x];
                    debugger;
                    this_1.servicioCaja.guardarProductosDesconectado(resultadoNuevo, nombreProcedimiento)
                        .subscribe(function (data) {
                        var tablaEliminar = data['_body'];
                        tablaEliminar = tablaEliminar.replace('[[', '[');
                        tablaEliminar = tablaEliminar.replace(']]', ']');
                        tablaEliminar = JSON.parse(tablaEliminar);
                        // let result = this._electronService.ipcRenderer.sendSync('eliminar-modo-desconectado',RUTA_MODO_DESCONECTADO,tablaEliminar)
                        if (valoresCantidadReal <= i) {
                            soloUnaVez += 1;
                            if (soloUnaVez == 1) {
                                _this.mensajeGeneral('Sincronizar información en el servidor principal', "\n                  La informaci\u00F3n de las p\u00E1gina " + cadenaMostrar + " se subi\u00F3 con exito al servidor principal y se elimino de la base de datos local.\n                  \"Recuerde que puede seguir ingresando registros en las p\u00E1ginas " + cadenaMostrar + " pero es recomendable que vuelva a sincronizar los registros de el servidor principal\"");
                                var datosFinales = [];
                                for (var i_1 = 0; i_1 < valores.length; i_1++) {
                                    var nombre_1 = valores[i_1].nombre + "Desconectado";
                                    datosFinales.push({
                                        tabla: valores[i_1].nombre + "Desconectado"
                                    });
                                }
                                var result_1 = _this._electronService.ipcRenderer.sendSync('eliminar-modo-desconectado', __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */], datosFinales);
                            }
                        }
                    }, function (error) {
                        console.log(error);
                        _this.mensajeGeneral('Guardar información', 'la información no se guardao verifique..');
                    });
                }
            }
        };
        var this_1 = this;
        for (var i = 0; i < valores.length; i++) {
            _loop_1(i);
        }
    };
    PrincipalPage.prototype.sincronizarDatosSubidosModoDesconectado = function () {
        var _this = this;
        var existe = this._electronService.ipcRenderer.sendSync('buscar-ventasDiariasSinSincronizar-modo-desconectado', __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */]);
        if (existe.length > 0) {
            this.storage.get('companiasId').then(function (companiasId) {
                var fecha = new Date();
                // var fechaSincroniza = ("0" + fecha.getDate()).slice(-2) + '-' + ("0" + (fecha.getMonth() + 1)).slice(-2) + '-' + fecha.getFullYear() + ' ' + ("0" + fecha.getHours()).slice(-2) + ':' + ("0" + fecha.getMinutes()).slice(-2) + ':' + ("0" + fecha.getSeconds()).slice(-2);
                var fechaSincroniza = (fecha.getFullYear() + '-' + ("0" + (fecha.getMonth() + 1)).slice(-2) + '-' + ("0" + fecha.getDate()).slice(-2) + ' ' + ("0" + fecha.getHours()).slice(-2) + ':' + ("0" + fecha.getMinutes()).slice(-2) + ':' + ("0" + fecha.getSeconds()).slice(-2));
                debugger;
                _this.servicioCaja.sincronizarDatosSubidosModoDesconectado(companiasId, fechaSincroniza)
                    .subscribe(function (data) {
                    debugger;
                    _this.mensajeGeneral('Sincronizar información', 'la información se proceso con exito..');
                    console.log(data);
                    _this.subirVentasDiarias();
                }, function (error) {
                    console.log(error);
                });
            });
        }
        else {
            this.mensajeGeneral('Sincronizar información', 'No existen ventas para sincronizar..');
        }
    };
    PrincipalPage.prototype.subirVentasDiarias = function () {
        var _this = this;
        var resultVenta = this._electronService.ipcRenderer.sendSync('buscar-ventasDiariasSinSincronizar-modo-desconectado', __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */]);
        if (resultVenta) {
            debugger;
            var _loop_2 = function (i) {
                var resultadoNuevo = resultVenta[i];
                data = resultadoNuevo;
                var laVenta = data.nombre;
                var jsonVenta = JSON.parse(laVenta);
                // let jsonVentaRecorrer = JSON.parse(jsonVenta.laVenta)
                // let jsonDatosAdicionalesRecorrer = JSON.parse(jsonVenta.datosAdicionales)
                debugger;
                this_2.servicioCaja.ventasDiariasSinSincronizar(jsonVenta)
                    .subscribe(function (data) {
                    if (resultVenta.length - 1 == i) {
                        var eliminar = _this._electronService.ipcRenderer.sendSync('eliminar-ventasDiariasSinSincronizar-modo-desconectado', __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */]);
                    }
                    console.log(data);
                }, function (error) {
                    console.log(error);
                });
            };
            var this_2 = this, data;
            for (var i = 0; i < resultVenta.length; i++) {
                _loop_2(i);
            }
        }
    };
    PrincipalPage.prototype.mensajeGeneral = function (titulo, mensaje) {
        this.AlertControllers.create({
            cssClass: 'ion-alert',
            title: titulo,
            message: mensaje,
            buttons: ['ok']
        }).present();
    };
    PrincipalPage.prototype.bajarVentas = function () {
        var _this = this;
        this.storage.get('companiasId').then(function (companiasId) {
            _this.servicioCompanias.guardarInformacionMododesconectado(companiasId)
                .subscribe(function (data) {
                var valores = _this._electronService.ipcRenderer.sendSync('guardar-informacion-mododesconectado', data);
                if (valores) {
                    _this.AlertControllers.create({
                        title: 'Sincronizar información',
                        message: 'la información se guardo con exito en su equipo local',
                        buttons: ['ok']
                    }).present();
                }
            }, function (error) {
                console.log(error);
            });
        });
    };
    PrincipalPage.prototype.verVersionamiento = function (item, imagen, canvas) {
        var modal = this.modalControllers.create(__WEBPACK_IMPORTED_MODULE_7__versionamiento_versionamiento__["a" /* VersionamientoPage */], { item: item, imagen: imagen, canvas: canvas });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                data.name = "";
            }
        });
    };
    return PrincipalPage;
}());
PrincipalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-principal',template:/*ion-inline-start:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\principal\principal.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <button ion-button menuToggle>\n                    <ion-icon name="menu"></ion-icon>\n                </button>\n        <ion-title>Menú principal</ion-title>\n        <ion-buttons end>\n        <button (click)="bajarVentas()" ion-button icon-only color="royal">\n                                <ion-icon name="cloudy"></ion-icon>\n                        </button>\n\n\n\n            <button (click)="subirVentasDesconect()" ion-button icon-only color="royal">\n                                <ion-icon name="cloud-upload"></ion-icon>\n                        </button> \n            <button (click)="cerrarSession()" ion-button icon-only color="royal">\n                                <ion-icon name="close-circle"></ion-icon>\n                            </button>\n      <button (click)="sincronizarDatosSubidosModoDesconectado()" ion-button icon-only color="royal">\n                                <ion-icon name="thunderstorm"></ion-icon>\n                        </button> \n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n    <div id="splash-background-image">\n        <img class="splash-screen" src="{{imagensplash}}" />\n        <img logo animation src="{{logoanimacion}}" />\n    </div>\n    <button ion-button menuToggle>\n                        <ion-icon name="menu"></ion-icon>\n                    </button>\n\n\n</ion-content>\n<ion-footer class="footer">\n    <div class="footer-left">\n        <h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n        <h6>Company @RestApp &copy; 2019</h6>\n        <h6>\n            <a class="correo" href="mailto:andresnaranjo@afnarqui.com">\n                <h6>andresnaranjo@afnarqui.com</h6>\n            </a>\n        </h6>\n        <h3 class="footer-center">Versión {{version}}\n            <ion-icon (click)="verVersionamiento()" name="search"></ion-icon><span></span></h3>\n    </div>\n\n\n</ion-footer>\n<!-- <ion-header>\n    <ion-navbar color="primary">\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n            <ion-title>Menú principal</ion-title>\n            <ion-buttons end>\n\n                    <button (click)="subirVentas()" ion-button icon-only color="royal">\n                            <ion-icon name="cloud-upload"></ion-icon>\n                    </button>\n                    <button (click)="cerrarSession()" ion-button icon-only color="royal">\n                        <ion-icon name="close-circle"></ion-icon>\n                    </button>\n                </ion-buttons>\n        </ion-navbar>\n\n    </ion-header>\n\n\n<ion-content padding>\n\n\n        <div id="splash-background-image">\n\n\n                <img class="splash-screen" src="{{imagensplash}}" />\n                <img logo animation src="{{logoanimacion}}" />\n              </div>\n                <button ion-button menuToggle>\n                        <ion-icon name="menu"></ion-icon>\n                </button>\n\n</ion-content>\n<ion-footer class="footer">\n        <div class="footer-left">\n                <h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n                <h6>Company @RestApp &copy; 2018</h6>\n                <h6><a class="correo" href="mailto:andresnaranjo@afnarqui.com"><h6>andresnaranjo@afnarqui.com</h6></a></h6>\n                <h3 class="footer-center">Versión {{version}} <span></span></h3>\n        </div>\n    </ion-footer> -->'/*ion-inline-end:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\principal\principal.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_caja_caja__["a" /* CajaProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__["a" /* CompaniasProvider */], __WEBPACK_IMPORTED_MODULE_5_ngx_electron__["a" /* ElectronService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
        __WEBPACK_IMPORTED_MODULE_2__providers_caja_caja__["a" /* CajaProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__["a" /* CompaniasProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5_ngx_electron__["a" /* ElectronService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
], PrincipalPage);

// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';
// import { MODO_DESCONECTADO,RUTA_MODO_DESCONECTADO,VERSION,NOMBRE_VERSION,DESCRIPCION_VERSION } from "../../config/url.servicio"
// import { CajaProvider } from '../../providers/caja/caja'
// @IonicPage()
// @Component({
//   selector: 'page-principal',
//template:/*ion-inline-start:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\principal\principal.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <button ion-button menuToggle>\n                    <ion-icon name="menu"></ion-icon>\n                </button>\n        <ion-title>Menú principal</ion-title>\n        <ion-buttons end>\n        <button (click)="bajarVentas()" ion-button icon-only color="royal">\n                                <ion-icon name="cloudy"></ion-icon>\n                        </button>\n\n\n\n            <button (click)="subirVentasDesconect()" ion-button icon-only color="royal">\n                                <ion-icon name="cloud-upload"></ion-icon>\n                        </button> \n            <button (click)="cerrarSession()" ion-button icon-only color="royal">\n                                <ion-icon name="close-circle"></ion-icon>\n                            </button>\n      <button (click)="sincronizarDatosSubidosModoDesconectado()" ion-button icon-only color="royal">\n                                <ion-icon name="thunderstorm"></ion-icon>\n                        </button> \n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n    <div id="splash-background-image">\n        <img class="splash-screen" src="{{imagensplash}}" />\n        <img logo animation src="{{logoanimacion}}" />\n    </div>\n    <button ion-button menuToggle>\n                        <ion-icon name="menu"></ion-icon>\n                    </button>\n\n\n</ion-content>\n<ion-footer class="footer">\n    <div class="footer-left">\n        <h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n        <h6>Company @RestApp &copy; 2019</h6>\n        <h6>\n            <a class="correo" href="mailto:andresnaranjo@afnarqui.com">\n                <h6>andresnaranjo@afnarqui.com</h6>\n            </a>\n        </h6>\n        <h3 class="footer-center">Versión {{version}}\n            <ion-icon (click)="verVersionamiento()" name="search"></ion-icon><span></span></h3>\n    </div>\n\n\n</ion-footer>\n<!-- <ion-header>\n    <ion-navbar color="primary">\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n            <ion-title>Menú principal</ion-title>\n            <ion-buttons end>\n\n                    <button (click)="subirVentas()" ion-button icon-only color="royal">\n                            <ion-icon name="cloud-upload"></ion-icon>\n                    </button>\n                    <button (click)="cerrarSession()" ion-button icon-only color="royal">\n                        <ion-icon name="close-circle"></ion-icon>\n                    </button>\n                </ion-buttons>\n        </ion-navbar>\n\n    </ion-header>\n\n\n<ion-content padding>\n\n\n        <div id="splash-background-image">\n\n\n                <img class="splash-screen" src="{{imagensplash}}" />\n                <img logo animation src="{{logoanimacion}}" />\n              </div>\n                <button ion-button menuToggle>\n                        <ion-icon name="menu"></ion-icon>\n                </button>\n\n</ion-content>\n<ion-footer class="footer">\n        <div class="footer-left">\n                <h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n                <h6>Company @RestApp &copy; 2018</h6>\n                <h6><a class="correo" href="mailto:andresnaranjo@afnarqui.com"><h6>andresnaranjo@afnarqui.com</h6></a></h6>\n                <h3 class="footer-center">Versión {{version}} <span></span></h3>\n        </div>\n    </ion-footer> -->'/*ion-inline-end:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\principal\principal.html"*/,
// })
// export class PrincipalPage {
//   imagensplash: string = !MODO_DESCONECTADO ? "assets/imgs/3.jpg" : "assets/imgs/3_desconectado.jpg"
//   logoanimacion: string = !MODO_DESCONECTADO ? "assets/imgs/logo.png" : "assets/imgs/logo_desconectado.png"
//   version:string = VERSION
//   nombreVersion:string = NOMBRE_VERSION
//   descripcionVersion:string = DESCRIPCION_VERSION
//   constructor(public navCtrl: NavController, public navParams: NavParams,
//              public alertControllers:AlertController,
//              public servicioCaja:CajaProvider){
//   }
//   ionViewDidLoad() {
//    // this.app.getRootNav().setRoot('PrincipalPage');
//   }
//   cerrarSession(){
//     this.navCtrl.setRoot('LoginPage');
//   }
//   subirVentas(){
//     // this.servicioCaja.buscarVentasModoDesconectado(RUTA_MODO_DESCONECTADO)
//     // this.alertControllers.create({
//     //   title: 'subir datos',
//     //   message: 'la información de los datos',
//     //   buttons: ['ok']
//     // }).present()
//   }
// }
//# sourceMappingURL=principal.js.map

/***/ })

});
//# sourceMappingURL=7.js.map