webpackJsonp([13],{

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CajaPageModule", function() { return CajaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_producto_dinamico_producto_dinamico__ = __webpack_require__(762);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__caja__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CajaPageModule = (function () {
    function CajaPageModule() {
    }
    return CajaPageModule;
}());
CajaPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__caja__["a" /* CajaPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_3__caja__["a" /* CajaPage */]),
            __WEBPACK_IMPORTED_MODULE_2__pages_producto_dinamico_producto_dinamico__["a" /* ProductoDinamicoPage */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__caja__["a" /* CajaPage */]
        ]
    })
], CajaPageModule);

//# sourceMappingURL=caja.module.js.map

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoDinamicoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_productos_por_grupos_productos_por_grupos__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProductoDinamicoPage = (function () {
    function ProductoDinamicoPage(navCtrl, navParams, servicio, servicioCompania, storage, alertControllers, auth, loadingCtrl, modalControllers, viewControllers) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.servicio = servicio;
        this.servicioCompania = servicioCompania;
        this.storage = storage;
        this.alertControllers = alertControllers;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.modalControllers = modalControllers;
        this.viewControllers = viewControllers;
        // data = []
        // constructor(public navCtrl: NavController, public navParams: NavParams) {
        //
        //   if(this.navParams.get('items')){
        //     this.data = this.navParams.get('items')
        //   }
        // }
        // ionViewDidLoad() {
        //   console.log('ionViewDidLoad ProductoDinamicoPage');
        // }
        this.listaProductos = [];
        this.productoAnterior = {};
        this.nombreCompaniaHeader = "";
        this.nombreCargoHeader = "";
        this.buscar = '';
        this.realizandoUnaVenta = true;
        this.queHago = true;
        this.data = [];
        this.valorProducto = 0;
        this.cantidadProducto = 1;
        this.productoNuevo = [];
        // for (let x = 0; x < 5; x++) {
        //   this.items.push(x);
        // }
        this.buscarporCompania('');
        this.servicioCompania.devolverVarios(['nombreCompania', 'nombreCargo', 'companiasId'])
            .then(function (val) {
            if (val) {
                _this.nombreCompaniaHeader = val['nombreCompania'];
                _this.nombreCargoHeader = val['nombreCargo'];
            }
        });
        this.buscarDatos();
    }
    ProductoDinamicoPage.prototype.buscarDatos = function () {
        var _this = this;
        this.storage.get('companiasId').then(function (val) {
            _this.companiasId = val;
            _this.servicio.buscarGruposCompaniasId(val)
                .subscribe(function (res) { return _this.listaGrupos = res; }, function (err) {
                var alert = _this.alertControllers.create({
                    title: 'Error',
                    subTitle: 'No se pudieron listar los grupos, intente luego',
                    buttons: ['Cerrar']
                });
                alert.present();
            });
            _this.servicio.buscarTodosLosProductosPorCompaniasDragAndDrop(val)
                .subscribe(function (res) { return _this.listaProductosSeleccionados = res; }, function (err) {
                var alert = _this.alertControllers.create({
                    title: 'Error',
                    subTitle: 'No se pudieron listar los productos, intente luego',
                    buttons: ['Cerrar']
                });
                alert.present();
            });
        });
    };
    ProductoDinamicoPage.prototype.reorderItems = function (indexes) {
        this.listaProductos = Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["reorderArray"])(this.listaProductos, indexes);
    };
    ProductoDinamicoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductosPorGruposDragPage');
    };
    ProductoDinamicoPage.prototype.agregarAlCarrito = function (item) {
        var productosIdBuscar = item['productoid'];
        var tipoProductoBuscar = item['tipo'];
        var contarProductosConElMismoId = this.listaProductos.filter(function (item) { return item.productoid === productosIdBuscar; });
        if (contarProductosConElMismoId.length > 0) {
            // el producto ya existe en el carrito
            return;
        }
        // el producto aun no ha sido agregado
        var valorVenta = item['valorVenta'];
        var cantidad = item['cantidad'];
        var valorSumar = valorVenta * cantidad;
        this.valorProducto += valorSumar;
        this.listaProductos.push(item);
        this.buscar = '';
        this.getItems(this.buscar);
        this.realizandoUnaVenta = true;
    };
    ProductoDinamicoPage.prototype.onEvent = function (event, item) {
        if (event === 'onTextChange') {
            var valor = this.getItems(this.buscar);
            if (valor) {
                item = valor;
            }
        }
    };
    ProductoDinamicoPage.prototype.getItems = function (event) {
        var _this = this;
        if (!this.allItems) {
            var valores = this.listaProductosSeleccionados;
            this.allItems = valores;
        }
        this.listaProductosSeleccionados = this.allItems.filter(function (item) {
            return item.nombreProducto.toLowerCase().indexOf(_this.buscar.toLowerCase()) > -1;
        });
    };
    ProductoDinamicoPage.prototype.cerrarSession = function () {
        this.auth.logout();
        this.navCtrl.setRoot('LoginPage');
    };
    ProductoDinamicoPage.prototype.guardar = function () {
        var _this = this;
        if (this.listaProductos.length > 0) {
            //   let data = `[
            //     {
            //        "productosElaborados":{
            //           "productosElaboradosId":"290",
            //           "producto":{
            //              "productosId":1864,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1869,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1870,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1886,
            //              "cantidad":1.000
            //           }
            //        }
            //     },
            //     {
            //        "productosElaborados":{
            //           "productosElaboradosId":"302",
            //           "producto":{
            //              "productosId":1864,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1865,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1866,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1867,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1882,
            //              "cantidad":2.000
            //           },
            //           "producto":{
            //              "productosId":2012,
            //              "cantidad":2.000
            //           }
            //        }
            //     }
            //  ]`
            //   let data = `[
            //     {
            //        "productosElaborados":{
            //           "productosElaboradosId":${this.productoAnterior['productosId']},
            //           "producto":{
            //              "productosId":1864,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1869,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1870,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1886,
            //              "cantidad":1.000
            //           }
            //        }
            //     },
            //     {
            //        "productosElaborados":{
            //           "productosElaboradosId":"302",
            //           "producto":{
            //              "productosId":1864,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1865,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1866,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1867,
            //              "cantidad":1.000
            //           },
            //           "producto":{
            //              "productosId":1882,
            //              "cantidad":2.000
            //           },
            //           "producto":{
            //              "productosId":2012,
            //              "cantidad":2.000
            //           }
            //        }
            //     }
            //  ]`
            // let dataReal = JSON.parse(data)
            var producto = '';
            var productoNuevo = '';
            var datos = this.listaProductos;
            for (var i = 0; i < datos.length; i++) {
                producto += "\"producto\": {\n            \"cantidad\":" + datos[i].cantidad + ",\n            \"imagen\":" + datos[i].imagen + ",\n            \"imagenLocal\":" + datos[i].imagenLocal + ",\n            \"nombreProducto\":" + datos[i].nombreProducto + ",\n            \"productosElaboradosId\":" + datos[i].productosElaboradosId + ",\n            \"productosId\":" + datos[i].productosId + ",\n            \"productoid\":" + datos[i].productosId + ",\n            \"idProductoDinamico\":" + datos[i].productosElaboradosId + ",\n            \"tipoProducto\":" + datos[i].tipoProducto + ",\n            \"valorImpuesto\":" + datos[i].valorImpuesto + ",\n            \"valorProducto\":" + datos[i].valorProducto + ",\n            \"valorVenta\":" + datos[i].valorVenta + ",\n            \"valorImpuestoCalculo\":" + datos[i].valorImpuestoCalculo + "\n           },";
                var idProductoDinamico = this.productoAnterior['productosId'];
                var productosElaboradosId = datos[i].productosElaboradosId == undefined ? 0 : datos[i].productosElaboradosId;
                var productosId_1 = datos[i].productoid == undefined ? datos[i].productosId : datos[i].productoid;
                var productoid = datos[i].productosElaboradosId == undefined ? 0 : datos[i].productosId;
                //  let tipoProducto = datos[i].productosElaboradosId == undefined ? "E" : datos[i].tipoProducto;
                var valorProducto = datos[i].productosElaboradosId == undefined ? 0 : datos[i].valorProducto;
                var valorImpuestoCalculo = datos[i].valorImpuestoCalculo == undefined ? 0 : datos[i].valorImpuestoCalculo;
                //  productoNuevo+= `{
                //   "cantidad":${datos[i].cantidad},
                //   "imagen":${JSON.stringify(datos[i].imagen)},
                //   "imagenLocal":${JSON.stringify(datos[i].imagenLocal)},
                //   "nombreProducto":${JSON.stringify(datos[i].nombreProducto)},
                //   "productosElaboradosId":${productosElaboradosId},
                //   "productosId":${productosId},
                //   "productoid":${productoid},
                //   "idProductoDinamico":${idProductoDinamico},
                //   "tipoProducto":"E",
                //   "valorImpuesto":${JSON.stringify(datos[i].valorImpuesto)},
                //   "valorProducto":${valorProducto},
                //   "valorVenta":${JSON.stringify(datos[i].valorVenta)},
                //   "valorImpuestoCalculo":${valorImpuestoCalculo}
                //  },`
                var existe = (datos[i].productosElaboradosOrigen === false || datos[i].productosElaboradosOrigen === undefined) ? 0 : datos[i].productosElaboradosOrigen;
                productoNuevo += "{\n            \"cantidad\":" + datos[i].cantidad + ",\n            \"productosId\":" + productosId_1 + ",\n            \"productosElaboradosOrigen\": " + existe + "\n           },";
                //  if(this.productoAnterior['productosId'] === dataReal[i]["productosElaborados"]["productosElaboradosId"]){
                // dataReal = {
                //   ...dataReal,
                // }
                //  }
            }
            var jsoncadena = producto;
            jsoncadena = jsoncadena.substr(jsoncadena.length - 1, 1) == ',' ? jsoncadena.substring(0, jsoncadena.length - 1) : jsoncadena;
            //  let nuevovalorjson =  "{" + jsoncadena + "}";
            var jsoncadenaProductoNuevo = productoNuevo;
            jsoncadenaProductoNuevo = jsoncadenaProductoNuevo.substr(jsoncadenaProductoNuevo.length - 1, 1) == ',' ? jsoncadenaProductoNuevo.substring(0, jsoncadenaProductoNuevo.length - 1) : jsoncadenaProductoNuevo;
            jsoncadenaProductoNuevo = "[" + jsoncadenaProductoNuevo + "]";
            this.productoNuevo = JSON.parse(jsoncadenaProductoNuevo);
            var nombreProducto = this.productoAnterior['nombreProducto'];
            var cantidad = this.productoAnterior['cantidad'];
            var productosId = this.productoAnterior['productosId'];
            var dataNue = "{\"productosElaborados\":{\n             \"productosElaboradosId\": " + productosId + " ,\n             \"nombreProducto\": " + nombreProducto + ",\n             \"cantidad\": " + cantidad + ",\n             " + jsoncadena + "}\n             ";
            var armarCadena = "[" + JSON.stringify(dataNue) + "]";
            var dataRecorrer = JSON.parse(armarCadena);
            var valoresRecorrer = [{
                    "productosElaboradosId": productosId,
                    "nombreProducto": nombreProducto,
                    "cantidad": cantidad,
                }];
            var valoresRecorrerCadena = JSON.stringify(valoresRecorrer);
            this.productoAnterior['valorVenta'] = this.valorProducto / this.productoAnterior['cantidad'];
            this.productoAnterior['productosElaboradosId'] = productosId;
            var a_1 = JSON.stringify(this.productoAnterior);
            // '{"companiasId":1,"productosElaboradosId":586,"usuariosId":4,"numeromesa":0,"estado":"A","fecha":"20190331","codigo":"AXXZ102030"}',
            // '[ {"productosId":2,"cantidad":3},{"productosId":3,"cantidad":2}]'
            this.servicioCompania.devolverVarios(['usuariosId', 'companiasId'])
                .then(function (val) {
                if (val) {
                    var usuariosId = val['usuariosId'];
                    var companiasId = val['companiasId'];
                    var data = _this.productoNuevo;
                    var dataInterior = JSON.parse(a_1);
                    var jsonValores = [];
                    jsonValores.push({
                        companiasId: companiasId,
                        productosElaboradosId: dataInterior.productosElaboradosId,
                        usuariosId: usuariosId,
                        numeromesa: 0,
                        estado: 'A'
                    });
                    debugger;
                    _this.servicio.guardarProductosElaboradosDinamicos(jsonValores, data, _this.cantidadProducto)
                        .subscribe(function (data) {
                        console.log(data);
                        var valorDevolvio = JSON.parse(data['_body']);
                        var dataNueva = valorDevolvio[0][0];
                        var miNuevoValor = JSON.stringify(dataNueva);
                        _this.storage.set('productosElaboradosContenidoDinamicoReemplazar', miNuevoValor);
                        _this.viewControllers.dismiss();
                    }, function (error) {
                        console.log(error);
                    });
                }
            });
            //  this.storage.get('productosElaboradosContenidoDinamico').then((val) => {
            //     if(val){
            //       let data = dataRecorrer
            //       this.viewControllers.dismiss();
            //     }else{
            //       this.storage.set('productosElaboradosContenidoDinamico',armarCadena)
            //       this.viewControllers.dismiss();
            //     }
            //  })
            //
            //  this.llenarListaProductos(jsoncadenaProductoNuevo);
            // if(this.gruposProductosId===undefined){
            //     this.mostrarMensajeAdvertencia('Debe seleccionar un grupo')
            //     this.buscar = ''
            //     this.getItems(this.buscar)
            //     this.realizandoUnaVenta = true
            // }else{
            //   let idgrupo = this.buscarUuidGrupos()
            //   let gruposProductosId = idgrupo[0].gruposProductosId
            //   for(let i=0;i<this.listaProductos.length;i++){
            //     let datosGruposProductos: any = {}
            //     let productosId = this.listaProductos[i].productoid
            //     if(productosId===undefined){
            //       productosId = this.listaProductos[i].productoId
            //     }
            //     let tipo = this.listaProductos[i].tipo
            //     let companiasId = this.listaProductos[i].companiasId
            //     let productosPorGruposId = this.listaProductos[i].productosPorGruposId
            //     datosGruposProductos = {
            //       gruposProductosId:   Number(gruposProductosId),
            //       tipoProducto: this.listaProductos[i].tipo === "P" ? "P" : "E" ,
            //       productoId: Number(productosId),
            //       companiasId:companiasId,
            //       queHago: this.listaProductos[i].productosPorGruposId>0 ? "A" : "I",
            //       nombreTabla: "productosPorGrupos",
            //       name: "dataDynamicCrud",
            //       orden:i
            //     }
            //     this.queHago = this.listaProductos[i].productosPorGruposId>0 ? false : true
            //     if(this.queHago){
            //       this.servicio.guardar(datosGruposProductos)
            //         .subscribe(
            //           res => this.procesarResultado(),
            //           err => this.mostrarMensajeDeError('No se pudo actualizar el producto, intente luego')
            //         )
            //     }else{
            //       datosGruposProductos['productosPorGruposId'] = Number(productosPorGruposId)
            //       this.servicio.actualizar(datosGruposProductos)
            //         .subscribe(
            //           res => this.procesarResultado(),
            //           err => this.mostrarMensajeDeError('No se pudo actualizar el producto, intente luego')
            //         )
            //     }
            //   }
            // }
        }
        else {
            var alert_1 = this.alertControllers.create({
                title: 'Advertencia',
                subTitle: 'Debe seleccionar un producto, verifique',
                buttons: ['Cerrar']
            });
            alert_1.present();
        }
        this.limpiar();
    };
    ProductoDinamicoPage.prototype.buscarUuidGrupos = function () {
        var data = [];
        var valorComparar = Number(this.gruposProductosId);
        for (var k = 0; k < this.listaGrupos.length; k++) {
            if (k === valorComparar) {
                data.push({
                    gruposProductosId: this.listaGrupos[k].gruposProductosId
                });
                k = this.listaGrupos.length + 1;
            }
        }
        return data;
    };
    ProductoDinamicoPage.prototype.limpiar = function () {
        this.listaProductos = [];
        this.realizandoUnaVenta = true;
        this.gruposProductosId = undefined;
        this.buscar = '';
    };
    ProductoDinamicoPage.prototype.mostrarMensajeDeError = function (mensaje) {
        var alert = this.alertControllers.create({
            title: 'Error',
            subTitle: mensaje,
            buttons: ['Cerrar']
        });
        alert.present();
        // this.loader.dismiss()
    };
    ProductoDinamicoPage.prototype.mostrarMensajeAdvertencia = function (mensaje) {
        var alert = this.alertControllers.create({
            title: 'Advertencia',
            subTitle: mensaje,
            buttons: ['Cerrar']
        });
        alert.present();
    };
    ProductoDinamicoPage.prototype.mostrarMensajeExito = function (mensaje) {
        var alert = this.alertControllers.create({
            title: 'Registro exitoso',
            subTitle: mensaje,
            buttons: ['Cerrar']
        });
        alert.present();
    };
    ProductoDinamicoPage.prototype.procesarResultado = function () {
        this.limpiar();
        // this.loader.dismiss()
        //   this.mostrarMensajeExito('sus registros fueron gestionados con exito')
        this.buscarDatos();
    };
    ProductoDinamicoPage.prototype.buscarporCompania = function (item) {
        var _this = this;
        // let idgrupo = this.buscarUuidGrupos()
        // let gruposProductosId = idgrupo[0].gruposProductosId
        // this.storage.get('companiasId').then((val)=>{
        //   let idgrupo = Number(gruposProductosId)
        //   this.servicio.buscarProductosPorGruposDrag(val,idgrupo)
        //     .subscribe((data)=>{
        //       this.listaProductos = data
        //     },(error)=>{
        //       console.log(error)
        //     })
        // })
        this.storage.get('productoAnterior').then(function (item) {
            if (item) {
                _this.productoAnterior = item;
                debugger;
                // this.valorProducto = item.cantidad * item.valorVenta
                _this.valorProducto = item.valorVenta;
                _this.cantidadProducto = item.cantidad;
                _this.id = item.productosId;
                var productosElaboradosDinamicosId_1 = 0;
                if (item.productosElaboradosDinamicosId !== undefined) {
                    productosElaboradosDinamicosId_1 = item.productosElaboradosDinamicosId;
                }
                debugger;
                _this.servicioCompania.devolverVarios(['usuariosId', 'companiasId'])
                    .then(function (val) {
                    if (val) {
                        var usuariosId = val['usuariosId'];
                        var companiasId = val['companiasId'];
                        _this.servicio.buscarProductosElaboradosDinamicos(companiasId, _this.id, usuariosId, 0, _this.cantidadProducto, productosElaboradosDinamicosId_1)
                            .subscribe(function (res) {
                            var valorReal = res[0]['ValorReal'];
                            if (valorReal === 'S') {
                                var data = res;
                                _this.listaProductos = res;
                                _this.valorProducto = _this.valorProducto * _this.cantidadProducto;
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].productosElaboradosOrigen) {
                                        _this.listaProductos[i].cantidad = _this.listaProductos[i].cantidad * _this.cantidadProducto;
                                        //this.listaProductos[i].valorVenta  = this.listaProductos[i].valorVenta * (this.listaProductos[i].cantidad)
                                    }
                                    else {
                                        //this.listaProductos[i].valorVenta  = this.listaProductos[i].valorVenta * (this.listaProductos[i].cantidad)
                                    }
                                }
                            }
                            else {
                                _this.listaProductos = res;
                                var data = _this.listaProductos;
                                _this.valorProducto = 0;
                                for (var i = 0; i < data.length; i++) {
                                    // this.listaProductos[i].valorVenta = data[i].valorVenta
                                    if (data[i].productosElaboradosOrigen) {
                                        // this.listaProductos[i].cantidad = this.listaProductos[i].cantidad
                                        _this.valorProducto += data[i].valorVenta * _this.listaProductos[i].cantidad;
                                    }
                                    else {
                                        _this.valorProducto += data[i].valorVenta * data[i].cantidad;
                                    }
                                }
                            }
                        }, function (err) {
                            var alert = _this.alertControllers.create({
                                title: 'Error',
                                subTitle: 'No se pudieron listar los productos, intente luego',
                                buttons: ['Cerrar']
                            });
                            alert.present();
                        });
                    }
                });
                // this.storage.get('ProductoDinamicoItem').then((data)=>{
                //
                //   if(data){
                //     let valores = data
                //     let id = this.id
                //     let datos =  JSON.parse(valores);
                //     let idComparar = Number(this.id)
                //     let existeEnLaData = 0;
                //     for(var i = 0 ; i< datos.length;i++){
                //       if(idComparar ===  datos[i].idProductoDinamico){
                //         existeEnLaData = 1
                //         this.listaProductos.push({
                //           "cantidad":datos[i].cantidad,
                //           "imagen":datos[i].imagen,
                //           "imagenLocal":datos[i].imagenLocal,
                //           "nombreProducto":datos[i].nombreProducto,
                //           "productosElaboradosId":datos[i].productosElaboradosId,
                //           "productosId":datos[i].productosId,
                //           "productoid":datos[i].productosId,
                //           "tipoProducto":datos[i].tipoProducto,
                //           "valorImpuesto":datos[i].valorImpuesto,
                //           "valorProducto":datos[i].valorProducto,
                //           "valorVenta":datos[i].valorVenta,
                //           "valorImpuestoCalculo":datos[i].valorImpuestoCalculo
                //         })
                //       } else {
                //         if(existeEnLaData === 0 && i + 1 == datos.length) {
                //           this.cargarListaProductosVacios()
                //         }
                //       }
                //     }
                //    }else {
                //     this.cargarListaProductosVacios()
                //    }
                // })
            }
        });
    };
    ProductoDinamicoPage.prototype.eliminarValor = function (item, i) {
        console.log(item, i);
    };
    ProductoDinamicoPage.prototype.eliminarRegistro = function (item, i) {
        var _this = this;
        if (item.productosPorGruposId !== 0) {
            this.servicio.eliminarProductosPorGrupoPorCompanias(item)
                .subscribe(function (data) {
                _this.listaProductos.splice(i, 1);
                console.log(data);
                var nuevaCantidad = Number(item['cantidad']);
                var nuevaValor = Number(item['valorVenta']);
                var valorReal = nuevaValor * nuevaCantidad;
                _this.valorProducto -= valorReal;
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.listaProductos.splice(i, 1);
            var nuevaCantidad = Number(item['cantidad']);
            var nuevaValor = Number(item['valorVenta']);
            var valorReal = nuevaValor * nuevaCantidad;
            this.valorProducto -= valorReal;
        }
    };
    // editarProductos(item: any) {
    //   let modal = this.modalControllers.create(ProductosEditarPage, { item })
    //   modal.present();
    //   modal.onDidDismiss(data => {
    //     if (data) {
    //       data.name = "dataDynamicCrud"
    //       data.productosId = data.productoId
    //       data.queHago = "A"
    //       data.nombreTabla = "productos"
    //       this.servicio.actualizar(data)
    //       .subscribe(
    //         res => {
    //            this.buscarDatos()
    //         },
    //         err => console.log(err)
    //       )
    //     }
    //   })
    // }
    ProductoDinamicoPage.prototype.disminuirUnProducto = function (item, index) {
        var nuevaCantidad = Number(item['cantidad']) - 1;
        // si la cantidad es menor o igual a cero debe quitar el producto
        if (nuevaCantidad <= 0) {
            item['cantidad'] = 1;
            //  this.listaProductosSeleccionados.splice(index, 1)
        }
        else {
            this.listaProductosSeleccionados[index]['cantidad'] = nuevaCantidad;
        }
    };
    ProductoDinamicoPage.prototype.disminuirUnProductoSeleccion = function (item, index) {
        var nuevaCantidad = Number(item['cantidad']) - 1;
        var nuevaValor = Number(item['valorVenta']);
        // si la cantidad es menor o igual a cero debe quitar el producto
        if (nuevaCantidad <= 0) {
            item['cantidad'] = 1;
            this.listaProductos.splice(index, 1);
        }
        else {
            this.listaProductos[index]['cantidad'] = nuevaCantidad;
        }
        if (this.valorProducto - nuevaValor < 0) {
            this.valorProducto = 0;
        }
        else {
            this.valorProducto -= nuevaValor;
        }
    };
    ProductoDinamicoPage.prototype.aumentarUnProducto = function (item, index) {
        var nuevaCantidad = Number(item['cantidad']) + 1;
        this.listaProductosSeleccionados[index]['cantidad'] = nuevaCantidad;
    };
    ProductoDinamicoPage.prototype.aumentarUnProductoSeleccion = function (item, index) {
        var nuevaCantidad = Number(item['cantidad']) + 1;
        var nuevaValorVenta = Number(item['valorVenta']);
        this.valorProducto += nuevaValorVenta;
        this.listaProductos[index]['cantidad'] = nuevaCantidad;
    };
    ProductoDinamicoPage.prototype.dismiss = function () {
        this.viewControllers.dismiss();
    };
    ProductoDinamicoPage.prototype.llenarListaProductos = function (producto) {
        // let dataCadena = JSON.stringify(producto)
        // let data = `${dataCadena}`
        this.storage.set('ProductoDinamicoItem', producto);
        // this.storage.get('ProductoDinamicoItem').then((data)=>{
        //   if(data){
        //
        //     let valores = data
        //     let datos =  JSON.parse(valores);
        //     for(var i = 0 ; i< datos.length;i++){
        //
        //       this.listaProductos.push({
        //         "cantidad":datos[i].cantidad * this.cantidadProducto,
        //         "imagen":datos[i].imagen,
        //         "imagenLocal":datos[i].imagenLocal,
        //         "nombreProducto":datos[i].nombreProducto,
        //         "productosElaboradosId":datos[i].productosElaboradosId,
        //         "productosId":datos[i].productosId,
        //         "productoid":datos[i].productosId,
        //         "tipoProducto":datos[i].tipoProducto,
        //         "valorImpuesto":datos[i].valorImpuesto,
        //         "valorProducto":datos[i].valorProducto,
        //         "valorVenta":datos[i].valorVenta
        //       })
        //     }
    };
    ProductoDinamicoPage.prototype.cargarListaProductosVacios = function () {
        var _this = this;
        this.storage.get('ProductoDinamicoItemSinModificar').then(function (data) {
            if (data) {
                var valores = data;
                var datos = JSON.parse(valores);
                for (var i = 0; i < datos.length; i++) {
                    _this.listaProductos.push({
                        "cantidad": datos[i].cantidad * _this.cantidadProducto,
                        "imagen": datos[i].imagen,
                        "imagenLocal": datos[i].imagenLocal,
                        "nombreProducto": datos[i].nombreProducto,
                        "productosElaboradosId": datos[i].productosElaboradosId,
                        "productosId": datos[i].productosId,
                        "productoid": datos[i].productosId,
                        "tipoProducto": datos[i].tipoProducto,
                        "valorImpuesto": datos[i].valorImpuesto,
                        "valorProducto": datos[i].valorProducto,
                        "valorVenta": datos[i].valorVenta,
                        "valorImpuestoCalculo": datos[i].valorImpuestoCalculo
                    });
                }
            }
        });
    };
    return ProductoDinamicoPage;
}());
ProductoDinamicoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-producto-dinamico',template:/*ion-inline-start:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\producto-dinamico\producto-dinamico.html"*/'<ion-header style="width:100% !important">\n    <!-- <ion-navbar color="primary">\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title> Producto Dinámico Drag and Drop\n            <div class="end"> {{nombreCargoHeader}} >> {{nombreCompaniaHeader}}</div>\n        </ion-title>\n        <ion-buttons end>\n            <button (click)="cerrarSession()" ion-button icon-only color="royal">\n            <ion-icon name="close-circle"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-navbar> -->\n    <ion-navbar color="primary">\n        <ion-title>Productos dinámico drag and drop\n            <div class="end"> {{nombreCargoHeader}} >> {{nombreCompaniaHeader}}</div>\n        </ion-title>\n\n        <ion-buttons end>\n            <button (click)="cerrarSession()" ion-button icon-only color="royal">\n                <ion-icon name="close-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n    <ion-toolbar no-padding>\n        <ion-searchbar [(ngModel)]="buscar" placeholder="Seleccione el producto que desee adicionar" (ionInput)="onEvent(\'onTextChange\', $event)"></ion-searchbar>\n    </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n    <div class="row responsive-sm">\n        <div class="col col-60">\n            <ion-list class="puntero">\n\n\n\n                <ion-item [style.background-color]="item.color" *ngFor="let item of listaProductosSeleccionados; let i = index">\n\n                    <ion-avatar item-start>\n\n                        <img *ngIf="MODO_DESCONECTADO" class="imgmoneda" src="{{item.imagenLocal}}">\n                        <img *ngIf="!MODO_DESCONECTADO" class="imgmoneda" src="{{item.imagen}}">\n                    </ion-avatar>\n\n                    <span item-end>\n\n                            <button ion-button (click)="disminuirUnProducto(item,i)">\n                              <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n                          </button>\n\n                          <button ion-button icon-only (click)="aumentarUnProducto(item,i)">\n                              <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon>\n                          </button>\n                               <ion-badge class="numero_cantidad"> {{item.cantidad}}  </ion-badge>\n                              <!-- <ion-icon name="send"></ion-icon> -->\n                              <ion-badge class="numero_total_precio"> {{item.cantidad * item.valorVenta | number}} </ion-badge>\n                          </span>\n\n                    <ion-label class="endCursorCenter" (click)="agregarAlCarrito(item)">\n                        <span class="endCursorCenter">{{item.nombreProducto}}</span>\n                    </ion-label>\n\n\n                </ion-item>\n\n\n\n            </ion-list>\n\n\n        </div>\n        <div class="col col-40">\n\n\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col col-1>\n                        <img class="imgs" src="{{ productoAnterior[\'imagen\'] }}" />\n                        <ion-badge class="numero_cantidadP"> {{productoAnterior[\'nombreProducto\']}} </ion-badge>\n                        <hr/>\n                        <ion-badge class="numero_cantidadT">Cantidad : {{productoAnterior[\'cantidad\']}} </ion-badge>\n                        <hr/>\n                        <ion-badge class="numero_cantidad">Valor : {{ valorProducto | number }} </ion-badge>\n\n                    </ion-col>\n\n                    <ion-col col col-4 class="flotar">\n\n                        <ion-fab bottom right *ngIf="listaProductos.length>0">\n                            <button ion-fab [disabled]="!realizandoUnaVenta" (click)="guardar()">Guardar</button>\n\n                        </ion-fab>\n                    </ion-col>\n                </ion-row>\n\n            </ion-grid>\n\n\n            <!-- <ion-grid>\n\n                <ion-row>\n                    <ion-col col col-1 style="font-size: 60px;">\n\n                        <hr/>\n\n                    </ion-col>\n                    <ion-col col col-2 style="font-size: 10px;">\n\n\n                    </ion-col>\n                    <ion-col col col-3 style="font-size: 5px;">\n\n\n\n                    </ion-col>\n\n\n                </ion-row>\n\n\n            </ion-grid> -->\n\n            <ion-list>\n                <!-- <ion-item *ngIf="listaProductos.length>0">\n                    <button ion-button full large [disabled]="!realizandoUnaVenta" (click)="guardar()">\n\n                          <ion-icon name="send">  Adicionar</ion-icon>\n                      </button>\n\n                </ion-item> -->\n\n\n                <!-- <ion-card>\n                    <ion-col>\n                        <img src="{{ productoAnterior[\'imagen\'] }}" />\n                        <ion-badge class="numero_cantidadP"> {{productoAnterior[\'nombreProducto\']}} </ion-badge>\n                        <ion-badge class="numero_cantidadT">Cantidad : {{productoAnterior[\'cantidad\']}} </ion-badge>\n                        <ion-badge class="numero_cantidad">Valor : {{ valorProducto }} </ion-badge>\n\n                    </ion-col>\n                    <ion-col>\n                        <ion-fab bottom side="left" *ngIf="listaProductos.length>0">\n                            <button ion-fab [disabled]="!realizandoUnaVenta" (click)="guardar()">Guardar</button>\n\n                        </ion-fab>\n                    </ion-col>\n                </ion-card> -->\n\n                <ion-item-group reorder="true" (ionItemReorder)="$event.applyTo(listaProductos)">\n\n                    <ion-item *ngFor="let item of listaProductos; let i=index">\n\n                        <ion-avatar item-start>\n\n                            <img *ngIf="MODO_DESCONECTADO" class="imgmoneda" src="{{item.imagenLocal}}">\n                            <img *ngIf="!MODO_DESCONECTADO" class="imgmoneda" src="{{item.imagen}}">\n                        </ion-avatar>\n                        <ion-label>\n                            <span>{{item.nombreProducto}}</span>\n                        </ion-label>\n                        <ion-label class="endCursor" (click)="eliminarRegistro(item,i)">\n                            <span> <ion-icon class="endCursor" name="trash"></ion-icon></span>\n                        </ion-label>\n                        <!-- <span item-end> -->\n                        <!-- <ion-badge class="numero_cantidad"> {{item.cantidad}} </ion-badge>\n\n                      </span> -->\n                        <span item-end>\n\n                        <button ion-button (click)="disminuirUnProductoSeleccion(item,i)">\n                          <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n                      </button>\n\n                      <button ion-button icon-only (click)="aumentarUnProductoSeleccion(item,i)">\n                          <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon>\n                      </button>\n                     \n                           <ion-badge class="numero_cantidad"> {{item.cantidad}}  </ion-badge>\n                           <ion-badge class="numero_total_precio"> {{item.cantidad * item.valorVenta | number}} </ion-badge>\n                      </span>\n\n\n\n                    </ion-item>\n\n\n\n\n                </ion-item-group>\n\n            </ion-list>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\producto-dinamico\producto-dinamico.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
        __WEBPACK_IMPORTED_MODULE_2__providers_productos_por_grupos_productos_por_grupos__["a" /* ProductosPorGruposProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_companias_companias__["a" /* CompaniasProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
], ProductoDinamicoPage);

//# sourceMappingURL=producto-dinamico.js.map

/***/ })

});
//# sourceMappingURL=13.js.map