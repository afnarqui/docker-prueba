webpackJsonp([11],{

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(769);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = (function () {
    function HomePageModule() {
    }
    return HomePageModule;
}());
HomePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */])
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
        ]
    })
], HomePageModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_clientes_clientes__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_companias_companias__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_url_servicio__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_reportes_reportes__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_seguridad_seguridad__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_caja_caja__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_meseromesero_meseromesero__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_versionamiento_versionamiento__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_push_push__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_push_push__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_Observable__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_rxjs_Observable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Chart } from 'chart.js';
// import { GraficasProvider } from '../../providers/graficas/graficas'

// import { ChatPage } from '../../pages/chat/chat'
// import { GraficasPage } from '../../pages/graficas/graficas'











var HomePage = (function () {
    function HomePage(navCtrl, auth, menuControllers, utils, storages, servicioCompania, alertControllers, modalControllers, servicioPush) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.menuControllers = menuControllers;
        this.utils = utils;
        this.storages = storages;
        this.servicioCompania = servicioCompania;
        this.alertControllers = alertControllers;
        this.modalControllers = modalControllers;
        this.servicioPush = servicioPush;
        this.data = [];
        this.datos = "";
        this.labels = "";
        this.dataReal = [{ valorReal: -1 }];
        this.nombreCompaniaHeader = "";
        this.nombreCargoHeader = "";
        this.imagensplash = !__WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["d" /* MODO_DESCONECTADO */] ? "assets/imgs/3.jpg" : "assets/imgs/3_desconectado.jpg";
        this.logoanimacion = !__WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["d" /* MODO_DESCONECTADO */] ? "assets/imgs/logo.png" : "assets/imgs/logo_desconectado.png";
        this.imagenInforme = 'https://res.cloudinary.com/drqk6qzo7/image/upload/v1534470788/informes-min_agpenr.jpg';
        this.version = __WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["k" /* VERSION */];
        this.nombreVersion = __WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["e" /* NOMBRE_VERSION */];
        this.descripcionVersion = __WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["a" /* DESCRIPCION_VERSION */];
        this.datosPaginaHome = [];
        this.recorrerVal = [];
        this.push = false;
        this.pushcantidad = 0;
        debugger;
        var mododesc = __WEBPACK_IMPORTED_MODULE_5__config_url_servicio__["d" /* MODO_DESCONECTADO */];
        if (!mododesc) {
            this.servicioCompania.devolverVarios(['nombreCompania', 'nombreCargo'])
                .then(function (val) {
                if (val) {
                    _this.buscarPush();
                    _this.nombreCompaniaHeader = val['nombreCompania'];
                    _this.nombreCargoHeader = val['nombreCargo'];
                }
            });
            this.datosPaginaHome = [];
            this.datosPaginaHome = [
                { "nombre": "CajaPage", "ruta": "CajaPage", "titulo": "Caja", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1511852931/caja_cs6wvs.jpg" },
                { "nombre": "MeseromeseroPage", "ruta": "MeseromeseroPage", "titulo": "Pedidos salón", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534613628/mesas-min_dnglao.jpg" },
                { "nombre": "SeguridadPage", "ruta": "SeguridadPage", "titulo": "Seguridad", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534611819/seguridad-min_dwzsgg.jpg" },
                { "nombre": "ReportesPage", "ruta": "ReportesPage", "titulo": "Informes", "imagen": "https://res.cloudinary.com/drqk6qzo7/image/upload/v1534470788/informes-min_agpenr.jpg" }
            ];
            this.storages.get('notificaciones').then(function (val) {
                if (val) {
                    if (val == "S") {
                        _this.startTimer();
                    }
                }
            });
        }
        else {
            debugger;
            this.navCtrl.setRoot('PrincipalPage');
        }
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.buscarPush();
    };
    HomePage.prototype.ngOnChanges = function (changes) {
        clearInterval(this.timer);
        this.executeEvents();
    };
    HomePage.prototype.getData = function () {
        return this.data;
    };
    HomePage.prototype.getEvents = function () {
        return this.events;
    };
    HomePage.prototype.executeEvents = function () {
        var duration = (this.getData() && this.getData().duration) ? this.getData().duration : 10000;
        var events = null;
        if (this.getEvents()) {
            events = this.getEvents()['onRedirect'];
        }
        this.timer = setTimeout(function () {
            if (events) {
                events();
            }
        }, duration);
    };
    HomePage.prototype.ngOnDestroy = function () {
        clearInterval(this.timer);
    };
    HomePage.prototype.chat = function () {
        this.navCtrl.push('ChatPage');
    };
    HomePage.prototype.abrirPaginaPermisos = function () {
        this.navCtrl.push('PermisosPage');
    };
    HomePage.prototype.graficas = function () {
        this.navCtrl.push('GraficasPage');
    };
    HomePage.prototype.sqlBuscar = function () {
        this.navCtrl.push('SqlPage');
    };
    HomePage.prototype.inventario = function () {
        this.navCtrl.push('inventarioPage');
    };
    HomePage.prototype.exportarventas = function () {
        this.navCtrl.push('exportarventasPage');
    };
    HomePage.prototype.ionViewWillLoad = function () {
        var _this = this;
        this.auth.authenticated()
            .then(function (autenticado) {
            if (!autenticado) {
                _this.navCtrl.setRoot('LoginPage');
            }
            _this.buscarPush();
        })
            .catch(function (err) {
            _this.navCtrl.setRoot('LoginPage');
        });
    };
    HomePage.prototype.buscarPush = function () {
        var _this = this;
        this.storages.get('companiasId').then(function (val) {
            if (val) {
                _this.servicioPush.buscar(val).subscribe(function (data) {
                    if (data[1][0].pushcantidad > 0) {
                        _this.pushcantidad = data[1][0].pushcantidad;
                    }
                    else {
                        _this.pushcantidad = 0;
                    }
                }, function (error) {
                    console.log(error);
                });
            }
        });
    };
    /**
     * @method cerrarSession este metodo limpia todas las variables de session y redirecciona al login
     */
    HomePage.prototype.cerrarSession = function () {
        this.auth.logout();
        this.navCtrl.setRoot('LoginPage');
    };
    HomePage.prototype.abrirPaginaDesdeMenu = function (ruta, nombre) {
        var _this = this;
        this.storages.get('rut').then(function (val) {
            _this.recorrerVal = JSON.parse(val);
            var data = 0;
            for (var i = 0; i < _this.recorrerVal.length; i++) {
                if (_this.recorrerVal[i].nombre == nombre) {
                    data = -1;
                }
            }
            if (data != -1) {
                _this.mostrarMensajes('No tiene permisos para ingresar', 'Verifique..');
            }
            else {
                _this.irArutas(ruta);
                _this.menuControllers.close();
            }
        });
    };
    HomePage.prototype.irArutas = function (ruta) {
        switch (ruta) {
            case "CajaPage":
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_caja_caja__["a" /* CajaPage */]);
                break;
            case "ReportesPage":
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_reportes_reportes__["a" /* ReportesPage */]);
                break;
            case "SeguridadPage":
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_seguridad_seguridad__["a" /* SeguridadPage */]);
                break;
            case "MeseromeseroPage":
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_meseromesero_meseromesero__["a" /* MeseromeseroPage */]);
                break;
        }
    };
    HomePage.prototype.mostrarMensajes = function (mensaje, advertencia) {
        var alert = this.alertControllers.create({
            title: advertencia,
            subTitle: mensaje,
            buttons: ['Ok']
        });
        alert.present();
    };
    HomePage.prototype.verVersionamiento = function (item, imagen, canvas) {
        var modal = this.modalControllers.create(__WEBPACK_IMPORTED_MODULE_11__pages_versionamiento_versionamiento__["a" /* VersionamientoPage */], { item: item, imagen: imagen, canvas: canvas });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                data.name = "";
            }
        });
    };
    HomePage.prototype.startTimer = function () {
        var _this = this;
        this.timerVar = __WEBPACK_IMPORTED_MODULE_14_rxjs_Observable__["Observable"].interval(600000).subscribe(function (x) {
            _this.procesarDatosPush();
        });
    };
    HomePage.prototype.procesarDatosPush = function () {
        var _this = this;
        this.storages.get('companiasId').then(function (val) {
            _this.servicioPush.procesarDatosPush(val)
                .debounceTime(3000)
                .subscribe(function (data) {
                var valor = data.json();
                console.log(valor);
                if (valor[1][0].pushcantidad > 0) {
                    _this.pushcantidad = valor[1][0].pushcantidad;
                }
            }, function (error) {
                console.log(error);
            });
        });
    };
    HomePage.prototype.pushMensaje = function () {
        var _this = this;
        this.push = !this.push;
        var datos = [];
        var modal = this.modalControllers.create(__WEBPACK_IMPORTED_MODULE_12__pages_push_push__["a" /* PushPage */], { datos: datos });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.buscarPush();
        });
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barCanvas'),
    __metadata("design:type", Object)
], HomePage.prototype, "barCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('barCanvasafn'),
    __metadata("design:type", Object)
], HomePage.prototype, "barCanvasafn", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('doughnutCanvas'),
    __metadata("design:type", Object)
], HomePage.prototype, "doughnutCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineCanvas'),
    __metadata("design:type", Object)
], HomePage.prototype, "lineCanvas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('data'),
    __metadata("design:type", Object)
], HomePage.prototype, "datas", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('events'),
    __metadata("design:type", Object)
], HomePage.prototype, "events", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\home\home.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title> Inicio\n\n               \n\n            <div class="end">\n\n                    <button ion-button icon-only (click)="pushMensaje()">\n\n                            <ion-icon name="notifications-outline"></ion-icon>\n\n                            <ion-badge *ngIf="pushcantidad!==0"\n\n                                       class="carrito-numero">\n\n                                    {{pushcantidad}}\n\n                            </ion-badge>\n\n                          </button>\n\n                {{nombreCargoHeader}} >> {{nombreCompaniaHeader}}</div></ion-title>\n\n  \n\n        <ion-buttons end>\n\n\n\n              \n\n               <button (click)="sqlBuscar()" ion-button icon-only color="royal">\n\n                        <ion-icon name="build"></ion-icon>\n\n                    </button> \n\n            \n\n                  \n\n            <button (click)="cerrarSession()" ion-button icon-only color="royal">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n\n\n \n\n \n\n              \n\n        </ion-buttons>\n\n\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n        \n\n<ion-content>\n\n      \n\n        <div id="splash-background-image">\n\n                <img class="splash-screen" src="{{imagensplash}}" />\n\n                <img logo animation src="{{logoanimacion}}" />\n\n        </div> \n\n\n\n          <ion-grid>\n\n            <ion-row style="cursor:pointer">\n\n              <ion-col col-12 col-md-6 col-lg-4 col-xl-3 *ngFor="let item of datosPaginaHome">\n\n                <ion-card (click)="abrirPaginaDesdeMenu(item.ruta,item.nombre)">\n\n                  <ion-card-header>{{item.titulo}}</ion-card-header>\n\n                  <ion-card-content>\n\n                        <img src="{{item.imagen}}" width="100" height="100"/>\n\n                  </ion-card-content>\n\n                </ion-card>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-grid>\n\n       \n\n        \n\n        <ion-fab bottom right >\n\n            <button ion-fab>Informes</button>\n\n        <ion-fab-list side="top">\n\n        \n\n          \n\n            \n\n        </ion-fab-list>\n\n        <ion-fab-list side="left">\n\n         \n\n            <button ion-button color="dark" (click)="inventario()">\n\n                <ion-icon name="pulse">  Actualizar Inventario</ion-icon>\n\n                 \n\n              </button>\n\n        \n\n              <button ion-button color="dark" (click)="graficas()">\n\n                <ion-icon name="pulse">  Ventas</ion-icon>\n\n                 \n\n              </button>\n\n        \n\n             \n\n        </ion-fab-list>\n\n        </ion-fab>   \n\n\n\n       \n\n              \n\n</ion-content>\n\n\n\n<ion-footer class="footer">\n\n    <div class="footer-left">\n\n            <h5>{{nombreVersion}} <span>{{descripcionVersion}}</span></h5>\n\n            <h6>Company @AppNegocio &copy; 2018</h6>\n\n            <h6><a class="correo" href="mailto:admin@viboafn.com"><h6>admin@viboafn.com</h6></a></h6>\n\n            <h3 class="footer-center">Versión {{version}} <ion-icon (click)="verVersionamiento()" name="search"></ion-icon> <span></span></h3>\n\n    </div>\n\n    \n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\home\home.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
        __WEBPACK_IMPORTED_MODULE_3__providers_clientes_clientes__["a" /* ClientesProvider */],
        __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_companias_companias__["a" /* CompaniasProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
        __WEBPACK_IMPORTED_MODULE_13__providers_push_push__["a" /* PushProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=11.js.map