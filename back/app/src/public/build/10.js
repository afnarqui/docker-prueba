webpackJsonp([10],{

/***/ 661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(764);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */])
        ]
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_companias_companias__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_electron__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var LoginPage = (function () {
    function LoginPage(alertCtrl, loadingCtrl, storage, navCtrl, navParams, auth, servicioCompanias, _electronService, modalControllers) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.servicioCompanias = servicioCompanias;
        this._electronService = _electronService;
        this.modalControllers = modalControllers;
        this.user = {};
        this.seleccionarCompania = false;
        this.companiasAcesso = [];
        this.entr = [];
        var mododesc = __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["d" /* MODO_DESCONECTADO */];
        if (mododesc) {
            console.log('entro en modo desconectado....');
            this.elect(false);
            this.elect(true);
            this.seleccionarCompania = true;
        }
        else {
            this.seleccionarCompania = false;
            this.auth.logout();
        }
    }
    LoginPage.prototype.elect = function (queHago) {
        var _this = this;
        var resultado;
        resultado = this._electronService.ipcRenderer.sendSync('buscarcompania-informacion-mododesconectado', __WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["f" /* RUTA_MODO_DESCONECTADO */]);
        if (queHago) {
            var users = this.presentPromp(resultado);
            if (!resultado) {
            }
            else {
            }
        }
        else {
            var resultadoerror = this._electronService.ipcRenderer.sendSync('buscarcompania-informacion-mododesconectado-error-unico', function (event, data) {
            });
            if (resultadoerror.length == 0) {
                var res = JSON.parse(resultado);
                this.seleccionarCompania = false;
                var companiaId = void 0;
                for (var index = 0; index < res.length; index++) {
                    companiaId = res[index]['companiasId'];
                }
                this.servicioCompanias.guardarInformacionMododesconectado(companiaId)
                    .subscribe(function (data) {
                    _this._electronService.ipcRenderer.send('guardar-informacion-mododesconectado', data);
                }, function (error) {
                    console.log(error);
                });
            }
            else {
            }
        }
    };
    LoginPage.prototype.presentPromp = function (resultado) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Login',
            inputs: [
                {
                    name: 'usuario',
                    placeholder: 'Usurio'
                },
                {
                    name: 'clave',
                    placeholder: 'Contraseña',
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        _this.presentPromp(resultado);
                    }
                },
                {
                    text: 'Entrar',
                    handler: function (data) {
                        var entr = _this._electronService.ipcRenderer.sendSync('buscarcompania-informacion-mododesconectado-error', data);
                        var res = JSON.parse(resultado);
                        if (entr.length > 0) {
                            var dataentr = entr;
                            var us = entr[0].usuariosId;
                            _this.storage.set('usuariosIdDesconect', us);
                            _this.seleccionarCompania = true;
                            for (var index = 0; index < res.length; index++) {
                                var nombre = res[index]['nombreCompania'];
                                var companiaId = res[index]['companiasId'];
                                _this.companiasAcesso.push({ id: companiaId, nombre: nombre, obj: res[index] });
                            }
                            for (var i = 0; i < dataentr.length; i++) {
                                var nombre = dataentr[i]['nombreCompania'];
                                var companiaId = dataentr[i]['companiaId'];
                                var cargosId = dataentr[i]['cargosId'];
                                _this.companiasAcesso.push({ id: companiaId, nombre: nombre, cargosId: cargosId, obj: dataentr[i] });
                            }
                            _this.companiasAcesso.splice(0, 1);
                            var datosrut = "[\n                {\"nombre\":\"ProductosElaboradosPage\"},\n                {\"nombre\":\"ProductosPorGruposPage\"},\n                {\"nombre\":\"ProductosPage\"},\n                {\"nombre\":\"UsuariosPage\"},\n                {\"nombre\":\"CajaPage\"},\n                {\"nombre\":\"CuadreCajaDiarioValoresPorDenominacionPage\"}\n                ,{\"nombre\":\"ProveedoresCargarStockPage\"},\n                {\"nombre\":\"GruposProductosPage\"},\n                {\"nombre\":\"ProveedoresPage\"}]";
                            _this.storage.set('rut', datosrut);
                            // this.storage.set('urlPrint', 'https://138.121.170.105:2347/print');
                            _this.storage.set('urlPrint', 'http://localhost:2347/print');
                            var data_1 = [{ valor: 0 }];
                            _this.storage.set('data', data_1);
                        }
                        else {
                            _this.presentPromp(resultado);
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    LoginPage.prototype.login = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var credenciales;
            return __generator(this, function (_a) {
                this.presentLoading();
                try {
                    credenciales = {
                        usuario: user.usuario,
                        clave: user.clave
                    };
                    this.companiasAcesso = [];
                    this.auth.login(credenciales).subscribe(function (res) {
                        _this.seleccionarCompania = true;
                        for (var index = 0; index < res.length; index++) {
                            var nombre = res[index]['nombreCompania'];
                            var companiaId = res[index]['companiasId'];
                            _this.companiasAcesso.push({ id: companiaId, nombre: nombre, obj: res[index] });
                        }
                        var posic = res.length - 1;
                        var datosrut = JSON.stringify(res[posic]);
                        _this.storage.set('rut', datosrut);
                        var data = [{ valor: 0 }];
                        _this.storage.set('data', data);
                        _this.loader.dismiss();
                    }, function (error) {
                        _this.loader.dismiss();
                        var message = JSON.parse(error['_body']);
                        _this.showAlert(message['message']);
                    });
                }
                catch (error) {
                    this.loader.dismiss();
                    this.showAlert(error['message']);
                    console.log(error);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push('RegisterPage');
    };
    LoginPage.prototype.showAlert = function (msj) {
        var alert = this.alertCtrl.create({
            title: '',
            subTitle: msj,
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            content: "",
            dismissOnPageChange: true,
        });
        this.loader.present();
    };
    LoginPage.prototype.limpiar = function () {
        this.seleccionarCompania = false;
        this.user.usuario = '';
        this.user.clave = '';
        this.user.nombreCompania = '';
        this.user.companiasId = 0;
    };
    LoginPage.prototype.ingresarEnCompania = function (obj) {
        if (!__WEBPACK_IMPORTED_MODULE_6__config_url_servicio__["d" /* MODO_DESCONECTADO */]) {
            this.auth.setVarsSession(obj);
            this.servicioCompanias.cargarcompaniaId(obj.companiasId);
            this.navCtrl.setRoot('HomePage');
        }
        else {
            this.auth.setVarsSession(obj);
            this.servicioCompanias.cargarcompaniaId(obj.companiasId);
            this.navCtrl.push('PrincipalPage');
        }
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\login\login.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title style="text-align: center;">\n\n      <!-- aqui va el logo de la app -->\n\n    </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid *ngIf="!seleccionarCompania">\n\n    <!-- digitar usuario -->\n\n    <ion-row justify-content-center>\n\n      <ion-col col-12 col-sm-9 col-md-7 col-lg-6>\n\n        <ion-item>\n\n          <ion-label>\n\n            <ion-icon ios="ios-contact" md="md-contact"></ion-icon>\n\n          </ion-label>\n\n          <ion-input type="text" clearInput [(ngModel)]="user.usuario"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <!-- digitar clave -->\n\n    <ion-row justify-content-center>\n\n      <ion-col col-12 col-sm-9 col-md-7 col-lg-6>\n\n        <ion-item>\n\n          <ion-label>\n\n            <ion-icon ios="ios-key" md="md-key"></ion-icon>\n\n          </ion-label>\n\n          <ion-input clearInput type="password" [(ngModel)]="user.clave"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row justify-content-center>\n\n      <ion-col col-12 col-sm-9 col-md-7 col-lg-6 style="text-align: center;">\n\n        <div padding>\n\n          <button large col-5 ion-button color="primary" (click)="login(user)">\n\n            Ingresar\n\n          </button>\n\n          <button large col-5 ion-button color="danger" (click)="limpiar()">\n\n              Limpiar todo\n\n          </button>\n\n          <!-- <button ion-button color="secondary" (click)="register()">Sign In</button> -->\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <ion-grid *ngIf="seleccionarCompania">\n\n    <!-- seleccionar compania -->\n\n    <ion-row justify-content-center>\n\n      <ion-col col-12 col-sm-9 col-md-7 col-lg-6>\n\n        <ion-item>\n\n          <H1 style="text-align: center;">Seleccione Compania a Ingresar</H1>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <!-- listado de companias disponibles -->\n\n    <ion-row justify-content-center>\n\n      <ion-col col-12 col-sm-9 col-md-7 col-lg-6>\n\n        <button ion-item color="primary" *ngFor="let item of companiasAcesso; let i = index" (click)="ingresarEnCompania(item.obj)">\n\n          {{ item.nombre }}\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer >\n\n  <div >\n\n      <!-- <a class="footer-center" href="https://marvelapp.com/9hdab37">Demo</a> -->\n\n      <a class="demo"  href="https://marvelapp.com/9hdab37">Demo <ion-icon name="search"></ion-icon> </a>\n\n      \n\n  </div>\n\n\n\n\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\learn\ResTiendAppModoDesconectadoElectron\bigdata-front-end - copia\src\pages\login\login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_4__providers_companias_companias__["a" /* CompaniasProvider */],
        __WEBPACK_IMPORTED_MODULE_5_ngx_electron__["a" /* ElectronService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=10.js.map