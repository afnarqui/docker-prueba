'use strict'

import config from '../config.js'
import jwt from 'jwt-simple'
import moment from 'moment'
/**
 * @class AuthService es la clase que se encarga de crear tokens y leerlos
 */
class AuthService {

    constructor() {}

    /**
     * @method encode este metodo se encarga de generar el webtoken
     * @param {object} user es un objecto que debe contener el id del usuario
     * @return retorna un string que es el token generado 
     */
    encode(user) {
        // crear el contenido del token antes de ser encodado
        const payload = {
            sub: user.usuariosId,
            iat: moment().unix(),
            exp: moment().add(1, 'days').unix()
        }
        // encodar el payload y retornar el token
        return jwt.encode(payload, config.pass);
    }

    /**
     * @method decode este metodo se encarga de decodificar un token enviado y validar
     * @param {string} token es el token que se va a decodificar
     * @return retornar una promesa 
     */
    decode(token) {
        return new Promise( (resolve, reject) => {
            // el codigo para decodificar puede generar errores para eso se mete a un try catch
            try {
                const payload = jwt.decode(token, config.pass)
                // verificar si el token ya caduco
                if ( payload.exp < moment().unix() ) {
                    reject({status: 401, message: 'El token ha expirado'})
                }
                // el token no ha caducado y todo esta ok, debe retornar el id del usuario
                resolve(payload.sub)

            } catch (error) {
                // hubo errores al momento de decodificar el token
                reject({status: 500, message: 'Error token'})
            }
        })
    }
}

export default AuthService