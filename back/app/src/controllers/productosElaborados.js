'use strict';

import sql from 'mssql'
import config from '../config.js'
import DB from '../conexion/db'
var multiparty = require('multiparty');

/**
 * @class productosElaborados este controlador se encarga de hacer todo el crud de productosElaborados
 */
class ProductosElaborados {

    constructor() {
        // libreria para subir las imagenes en el cloud y obtener solo su ruta
        this.cloudinary = require('cloudinary');
        // configurar clodinary con las credenciales en el archivo config
        this.cloudinary.config(config.cloudinary);
    }

    /**
     * @method crear es el metodo que se encarga de registar nuevos productosElaborados
     * @param {object} req es el objecto que contiene toda la informacion enviada desde el front
     * @param {object} res es el objecto que contiene los metodos para reponder la peticion 
     */
    crear(req, res) {
        debugger
        var form = new multiparty.Form()

        form.parse(req, (err, fields, files) => {
            var imagen = files.imagen
            var singleFile = imagen[0]

            this.cloudinary.uploader.upload(singleFile['path'], function(result) { 
                
                var productosElaborados = {}
                productosElaborados.codigoProductoElaborado = fields.codigoProducto[0]
                productosElaborados.nombreProductoElaborado = fields.nombreProducto[0]
                productosElaborados.valorVenta = fields.valorVenta[0]
                productosElaborados.companiasId = fields.companiasId[0]
                productosElaborados.tipoImpuesto = fields.tipoImpuesto[0]
                productosElaborados.valorImpuesto = fields.valorImpuesto[0]
                productosElaborados.imagen = result.url

                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(productosElaborados), nombreTabla: 'productosElaborados', queHago: 'I' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            })
        })
    }

    editar(req, res) {
        debugger
        var form = new multiparty.Form()
        debugger
        // this.cloudinary.uploader.upload(singleFile['path'], function(result) { 
        form.parse(req, (err, fields, files) => {
            
        debugger
            var productosElaborados = {}
            productosElaborados.codigoProductoElaborado = fields.codigoProducto[0]
            productosElaborados.nombreProductoElaborado = fields.nombreProducto[0]
            productosElaborados.valorVenta = fields.valorVenta[0]
            productosElaborados.companiasId = fields.companiasId[0]
            productosElaborados.tipoImpuesto = fields.tipoImpuesto[0]
            productosElaborados.valorImpuesto = fields.valorImpuesto[0]
            // productosElaborados.imagen = result.url
            productosElaborados.imagen = files.imagen
            productosElaborados.productosElaboradosId = fields.productosElaboradosId[0]

            var imagen = files['imagen']
            if(imagen){
                var singleFile = imagen[0]
                this.cloudinary.uploader.upload(singleFile['path'], function(result) { 

                    productosElaborados.imagen = result.url
                    // conectar con la base de datos
                    const sql = new DB()
                    // agregar campos para el crud dinamico de guardar
                    //guarda reutilizando el crud dinamico
                    sql.Procedure('dataDynamicCrud', {json: JSON.stringify(productosElaborados), nombreTabla: 'productosElaborados', queHago: 'A' })
                    .then( response => {
                        res.send(response)
                    })
                    .catch( error => {
                        res.status(500).send({error: 500, mensaje:'Error'})
                    })
                })
            } else {
                // no se modifico la imagen
                productosElaborados.imagen = fields.imagen[0]
                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(productosElaborados), nombreTabla: 'productosElaborados', queHago: 'A' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            }
        })
    }
    
}

export default ProductosElaborados;