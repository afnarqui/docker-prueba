'use strict';

import sql from 'mssql'
import config from '../config.js'
import DB from '../conexion/db'
// var formidable=require('../../lbl/formidable');
// var formidable = require('formidable');

var multiparty = require('multiparty');

/**
 * @class Productos este controlador se encarga de hacer todo el crud de productos
 */
class Productos {

    constructor() {
        // libreria para subir las imagenes en el cloud y obtener solo su ruta
        this.cloudinary = require('cloudinary');
        // configurar clodinary con las credenciales en el archivo config
        this.cloudinary.config(config.cloudinary);
    }

    /**
     * @method crear es el metodo que se encarga de registar nuevos productos
     * @param {object} req es el objecto que contiene toda la informacion enviada desde el front
     * @param {object} res es el objecto que contiene los metodos para reponder la peticion 
     */
    crear(req, res) {
 
        var form = new multiparty.Form()
        
        form.parse(req, (err, fields, files) => {
            var imagen = files.imagen
            var singleFile = imagen[0]

            this.cloudinary.uploader.upload(singleFile['path'], function(result) { 
     
                var producto = {}
                producto.codigoProducto = fields.codigoProducto[0]
                producto.nombreProducto = fields.nombreProducto[0]
                producto.descripcion = fields.descripcion[0]
                producto.imagen = result.url
                producto.costo = fields.costo[0]
                producto.valorVenta = fields.valorVenta[0]
                producto.existenciasMinimas = fields.existenciasMinimas[0]
                producto.existenciasActuales = fields.existenciasActuales[0]
                producto.visualizaEnCaja = fields.visualizaEnCaja[0]
                producto.controlaEnInventario = fields.controlaEnInventario[0]
                producto.companiasId = fields.companiasId[0]
                producto.tipoImpuesto = fields.tipoImpuesto[0]
                producto.valorImpuesto = fields.valorImpuesto[0]
                producto.centroDeCostosId = fields.centroDeCostosId[0]
                producto.codigoBarra =  fields.codigoBarra[0]
                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(producto), nombreTabla: 'productos', queHago: 'I' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            })
        })
    }


    editar(req, res) {

        var form = new multiparty.Form()

        form.parse(req, (err, fields, files) => {
           
            var producto = {}
            producto.codigoProducto = fields.codigoProducto[0]
            producto.nombreProducto = fields.nombreProducto[0]
            producto.descripcion = fields.descripcion[0]
            producto.costo = fields.costo[0]
            producto.valorVenta = fields.valorVenta[0]
            producto.existenciasMinimas = fields.existenciasMinimas[0]
            producto.existenciasActuales = fields.existenciasActuales[0]
            producto.visualizaEnCaja = fields.visualizaEnCaja[0]
            producto.controlaEnInventario = fields.controlaEnInventario[0]
            producto.companiasId = fields.companiasId[0]
            producto.productosId = fields.productosId[0]
            producto.tipoImpuesto = fields.tipoImpuesto[0]
            producto.valorImpuesto = fields.valorImpuesto[0]
            producto.centroDeCostosId = fields.centroDeCostosId[0]
            producto.codigoBarra =  fields.codigoBarra[0]
            
            var imagen = files['imagen']
            if(imagen){
        
                var singleFile = imagen[0]


                this.cloudinary.uploader.upload(singleFile['path'], function(result) { 


                    producto.imagen = result.url
                    // conectar con la base de datos
                    const sql = new DB()
                    // agregar campos para el crud dinamico de guardar
                    //guarda reutilizando el crud dinamico
                    sql.Procedure('dataDynamicCrud', {json: JSON.stringify(producto), nombreTabla: 'productos', queHago: 'A' })
                    .then( response => {
                        res.send(response)
                    })
                    .catch( error => {
                        res.status(500).send({error: 500, mensaje:'Error'})
                    })
                })
            } else {
                // no se modifico la imagen
                producto.imagen = fields.imagen[0]
                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(producto), nombreTabla: 'productos', queHago: 'A' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            }
        })
    }
    
}

export default Productos;