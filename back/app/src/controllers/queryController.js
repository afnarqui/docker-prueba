import modelQuery from '../models/query'

const query = {}

query.get = (req, res, params) => {
    modelQuery.get(req, res, params)
}

query.post = (req, res, params) => {
    modelQuery.post(req, res, params)
}

query.postr = (req, res, params) => {
    modelQuery.postr(req, res, params)
}

query.delete = (req, res, params) => {
    modelQuery.delete(req, res, params)
}

query.put = (req, res, params) => {
    modelQuery.put(req, res, params)
}

module.exports = query