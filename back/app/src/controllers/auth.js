'use strict';

import sql from 'mssql'
import config from '../config.js'
import bcrypt from 'bcrypt-nodejs'
import AuthService from '../services/auth'
import DB from '../conexion/db'

/**
 * @class Auth es la clase que contiene los metodos para guardar el usuario
 */
class Auth {

    constructor() {
        // inicializar el auth service que nos permite codificar y decodificar tokens
        this.authService = new AuthService()
    }

    /**
     * @method signUp es el metodo que se encarga de registar usuarios nuevos
     * @param {object} req es el objecto que contiene toda la informacion enviada desde el front
     * @param {object} res es el objecto que contiene los metodos para reponder la peticion 
     */
    signUp(req, res) {
        debugger
        // conectar con la base de datos
        const sql = new DB()
            // obtener la informacion del usuario
        let jsonUser = this.parseUser(req.body)

        sql.Procedure('registroUsuario', { 'json': JSON.stringify(jsonUser), 'queHago': 'I' })
            .then(response => {

                res.send(response)
            })
            .catch(error => {

                res.status(500).send({ error: 500, mensaje: 'Error' })
            })
    }

    /**
     * @method signIn es el metodo que se encarga de autenticar al usuario
     * @param {object} req 
     * @param {object} res 
     */
    singIn(req, res) {
        // ir a base de datos y verificar si el usuario existe y validar con su clave
        // conectar con la base de datos y retornar la informacion

        const sql = new DB()
        sql.Procedure('loginUsuarios', { CodigoUsuario: req.body['usuario'] })
            .then(response => {

                let responseData = []
                if (response[0][0] !== undefined) {
                    responseData = response[1]
                    response = response[0]
                }

                // verificar si hay datos y si tiene el campo clave
                if (response.length > 0 && response[0]['clave']) {

                    // verificar si la clave es valida para el usuario seleccionado
                    let hash = response[0]['clave']
                    let validPass = bcrypt.compareSync(req.body['clave'], hash)
                        // si la clave no es valida debe retornar clave incorrecta o usuario
                    if (!validPass) {
                        res.status(401).send({ status: 401, message: 'usuario o clave no valido' })
                    }

                    //eliminar la clave del objecto retornado por la base de datos y crear el token
                    let token = this.authService.encode(response[0])
                        // eliminar la clave hash y agregar el token

                    for (let i = 0; i < response.length; i++) {
                        delete response[i]['clave']
                        response[i]['token'] = token
                    }

                    // retornar las sucursales, informacion y el token
                    response.push(responseData)
                    res.send(response)

                } else {
                    // usuario no valido

                    res.status(401).send({ status: 401, message: 'usuario o clave no valido' })
                }
            })
            .catch(error => {
                // retornar el error en la peticion
                res.status(500).send({ error: 500, mensaje: 'Error autenticacion' })
            })
    }

    /**
     * @method parseUser se encarga de recorrer la informacion del usuario y encriptar la clave
     * @param {object} obj son los parametros enviados al controlador por post 
     * @return retorna un objecto con toda la infromacion del usuario para autenticar o registrar
     */
    parseUser(obj) {
        // recorrer los datos enviados desde el frontend para enviarlos a sql
        let jsonUser = {}
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {

                // si el campo se llama clave debe encriyptar
                if (key === 'clave') {
                    obj[key] = bcrypt.hashSync(obj[key].toString())
                }

                jsonUser[key] = obj[key]
            }
        }

        return jsonUser
    }
}

export default Auth;