'use strict';

import sql from 'mssql'
import config from '../config.js'
import DB from '../conexion/db'
var multiparty = require('multiparty');

/**
 * @class GruposProductos este controlador se encarga de hacer todo el crud de gruposProductos
 */
class GruposProductos {

    constructor() {
        // libreria para subir las imagenes en el cloud y obtener solo su ruta
        this.cloudinary = require('cloudinary');
        // configurar clodinary con las credenciales en el archivo config
        this.cloudinary.config(config.cloudinary);
    }

    /**
     * @method crear es el metodo que se encarga de registar nuevos grupos productos
     * @param {object} req es el objecto que contiene toda la informacion enviada desde el front
     * @param {object} res es el objecto que contiene los metodos para reponder la peticion 
     */
    crear(req, res) {
        
        var form = new multiparty.Form()

        form.parse(req, (err, fields, files) => {
            var imagen = files.imagen
            var singleFile = imagen[0]

            this.cloudinary.uploader.upload(singleFile['path'], function(result) { 
                
                var gruposProductos = {}
                gruposProductos.codigoGrupoProducto = fields.codigoGrupoProducto[0]
                gruposProductos.nombreGrupoProducto = fields.nombreGrupoProducto[0]
                gruposProductos.imagen = result.url
                gruposProductos.companiasId = fields.companiasId[0]
                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(gruposProductos), nombreTabla: 'gruposProductos', queHago: 'I' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            })
        })
    }

    editar(req, res) {
        var form = new multiparty.Form()
        
        form.parse(req, (err, fields, files) => {

            var gruposProductos = {}
            gruposProductos.codigoGrupoProducto = fields.codigoGrupoProducto[0]
            gruposProductos.nombreGrupoProducto = fields.nombreGrupoProducto[0]
            gruposProductos.companiasId = fields.companiasId[0]
            gruposProductos.gruposProductosId = fields.gruposProductosId[0]

            var imagen = files['imagen']
            if(imagen){
                var singleFile = imagen[0]
                this.cloudinary.uploader.upload(singleFile['path'], function(result) { 

                    gruposProductos.imagen = result.url
                    // conectar con la base de datos
                    const sql = new DB()
                    // agregar campos para el crud dinamico de guardar
                    //guarda reutilizando el crud dinamico
                    sql.Procedure('dataDynamicCrud', {json: JSON.stringify(gruposProductos), nombreTabla: 'gruposProductos', queHago: 'A' })
                    .then( response => {
                        res.send(response)
                    })
                    .catch( error => {
                        res.status(500).send({error: 500, mensaje:'Error'})
                    })
                })
            } else {
                // no se modifico la imagen
                gruposProductos.imagen = fields.imagen[0]
                // conectar con la base de datos
                const sql = new DB()
                // agregar campos para el crud dinamico de guardar
                //guarda reutilizando el crud dinamico
                sql.Procedure('dataDynamicCrud', {json: JSON.stringify(gruposProductos), nombreTabla: 'gruposProductos', queHago: 'A' })
                .then( response => {
                    res.send(response)
                })
                .catch( error => {
                    res.status(500).send({error: 500, mensaje:'Error'})
                })
            }
        })
    }
    
}

export default GruposProductos;