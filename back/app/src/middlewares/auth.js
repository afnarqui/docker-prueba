'use stric'

import servicesAuth from '../services/auth'

/**
 * @method isAuth es un middlewre que se encarga de validar y autorizar rutas
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function isAuth(req, res, next) {


    const authService = new servicesAuth()
    // si no llega el token debe retornar
    if (!req.headers.authorization) {
        return res.status(403).send({message: 'No tienes autorización'})
    }
    // por defecto el token debe llegar con el bearer ejemplo Bearrer $829340482msk8242a12
    const token = req.headers.authorization.split(' ')[1]

    authService.decode(token)
        .then( response => {
            req.user = response
            next()
        })
        .catch( error => {
            res.status(error.status).send(error.message)
        })
}

module.exports = isAuth